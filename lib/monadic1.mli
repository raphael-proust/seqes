(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Simon Cruanes                                          *)
(*                                                                        *)
(*   Copyright 2017 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*   Copyright 2025 Raphaël Proust                                        *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(** Monad functors over Stdlib Sequences

   {!Stdlib.Seq} provides an abstraction for delayed lists. One limitation of
   the {!Stdlib.Seq} module is that its type cannot accommodate monads. For
   example, considering a cooperative I/O monad in the style of Async or Lwt,
   using the type ['a io] to denote promises of ['a], one cannot write
   {[
     val map_io : ('a -> 'b io) -> 'a Seq.t -> 'b Seq.t
   ]}

   That's because the [Seq.t] type includes an arrow ([->]) that is outside of
   the monad, as per the type declaration.

   This here module exposes a [Make] functor which produces new [Seq]-like
   modules with a baked-in monad.

   Recommended readings:

   - Familiarity with {!Stdlib.Seq} is assumed.
   - See {!Sigs1} for an explanation of the separation of Traversors,
   Transformers, and the rest.

   *)

(** The module type for the output of the {!Make} module (below). *)
module type S1 = sig

  (** The type of the monad that the specialised Seq variation is built upon.
      This type is destructively substituted by the application of the functor;
      it is replaced by the actual monad type passed as parameter. *)
  type 'a mon

  (** The Seq-like type: identical to [Stdlib.Seq.t] except for [mon]. *)
  type 'a t = unit -> 'a node mon
  and 'a node =
    | Nil
    | Cons of 'a * 'a t

  (** This [include] brings all the functions from the [Stdlib.Seq] module but
      specialised to the specialised Seq variation. E.g., given
      [module SeqMon = Make(Mon)] then the function [SeqMon.map] has type
      [('a -> 'b) -> 'a t -> 'b t] and [SeqMon.iter] has type
      [('a -> unit) -> 'a t -> unit mon].

      See the documentation of {!Sigs1.SEQMON1ALL} for more details. *)
  include Sigs1.SEQMON1ALL
    with type 'a mon := 'a mon
    with type 'a t := 'a t

  (** [M] is a module which contains a specialised subset of the functions from
      the [Stdlib.Seq] module. Specifically, it contains those functions which
      take a function as parameter (e.g., [map] but not [length]). Moreover,
      those parameter functions' return type is specialised to be within the
      [mon] monad. E.g., given [module SeqMon = Make(Mon)] then [SeqMon.M.map]
      has type [('a -> 'b Mon.t) -> 'a t -> 'b t] and [SeqMon.M.iter] has type
      [('a -> unit mon) -> 'a t -> unit mon].

      See the documentation of {!Sigs1.SEQMON1TRANSFORMERS} for more details. *)
  module M
  : Sigs1.SEQMON1TRANSFORMERS
      with type 'a mon := 'a mon
      with type 'a callermon := 'a mon
      with type 'a t := 'a t

  (** [Make] is a functor to produce further [M]-like modules, but with
      parameter functions returning into a different monad. E.g., given
      [module SeqMon = Make(Mon)] and
      [module SeqMonMun = SeqMon.Make(Mun)] then [SeqMonMun.map] has type
      [('a -> 'b Mun.t) -> 'a t -> 'b t].

      Note that the functor parameter includes the necessary machinery to bundle
      the two monads together.

      See the documentation of {!Sigs1.SEQMON1TRANSFORMERS} for more details. *)
  module Make
    (Alt : Sigs1.MONAD1)
    (Glue : Sigs1.GLUE1
      with type 'a x := 'a Alt.t
      with type 'a f := 'a mon
      with type 'a ret := 'a mon)
  : Sigs1.SEQMON1TRANSFORMERS
      with type 'a mon := 'a mon
      with type 'a callermon := 'a Alt.t
      with type 'a t := 'a t

  (** [MakeTraversors] is a functor similar to [Make]. It produces only a subset
      of the functions that [Make] does. Specifically, it produces the subset of
      functions that traverse a sequence (or part thereof) and return a value
      which is not a sequence (e.g., [iter] returning [unit] but not [map]
      returning a new sequence).

      In this subset it is possible to mix the monad in more complex ways. And
      thus the monad-combining machinery provided as functor parameter is more
      complex.

      See [examples/] for use.

      See the documentation of {!Sigs1.SEQMON1TRAVERSORS} for more details. *)
  module MakeTraversors
    (Alt : Sigs1.MONAD1)
    (Ret : Sigs1.MONAD1)
    (GlueAlt : Sigs1.GLUE1
      with type 'a x := 'a Alt.t
      with type 'a f := 'a Ret.t
      with type 'a ret := 'a Ret.t)
    (GlueMon : Sigs1.GLUE1
      with type 'a x := 'a mon
      with type 'a f := 'a Ret.t
      with type 'a ret := 'a Ret.t)
  : Sigs1.SEQMON1TRAVERSORS
      with type 'a mon := 'a Ret.t
      with type 'a callermon := 'a Alt.t
      with type 'a t := 'a t

  (** [MakeTraversors2] is a functor similar to [MakeTraversors] but for a
      two-parameter monad.

      See [examples/seqlwt/] for use.

      See the documentation of {!Sigs2.SEQMON2TRAVERSORS} for more details. *)
  module MakeTraversors2
    (Alt : Sigs2.MONAD2)
    (Ret : Sigs2.MONAD2)
    (GlueAlt : Sigs2.GLUE2
      with type ('a, 'e) x := ('a, 'e) Alt.t
      with type ('a, 'e) f := ('a, 'e) Ret.t
      with type ('a, 'e) ret := ('a, 'e) Ret.t)
    (GlueMon : Sigs2.GLUE2
      with type ('a, 'e) x := 'a mon
      with type ('a, 'e) f := ('a, 'e) Ret.t
      with type ('a, 'e) ret := ('a, 'e) Ret.t)
  : Sigs2.SEQMON2TRAVERSORS
      with type ('a, 'e) mon := ('a, 'e) Ret.t
      with type ('a, 'e) callermon := ('a, 'e) Alt.t
      with type ('a, 'e) t := 'a t
end

(** Make a specialised Seq-like module based on a given Monad. *)
module Make1
  (Mon: Sigs1.MONAD1)
: S1
   with type 'a mon := 'a Mon.t
