(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Simon Cruanes                                          *)
(*                                                                        *)
(*   Copyright 2017 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*   Copyright 2025 Raphaël Proust                                        *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

type (+'a, +'t) node =
  | Nil of 't
  | Cons of 'a * ('a, 't) t

and ('a, 't) t = unit -> ('a, 't) node

let rec to_seq s f () =
  match s () with
  | Nil t -> begin
      match f t with
      | None -> Seq.Nil
      | Some x -> Seq.Cons (x, fun () -> Seq.Nil)
  end
  | Cons (x, s) -> Seq.Cons (x, to_seq s f)

let rec of_seq xs t () =
  match xs () with
  | Seq.Nil -> Nil t
  | Seq.Cons (x, xs) -> Cons (x, of_seq xs t)

type uninhabited = |

let terminated t () = Nil t

let cons x next () = Cons (x, next)

let rec append seq1 seq2 () =
  match seq1() with
  | Nil t -> seq2 t ()
  | Cons (x, next) -> Cons (x, append next seq2)

let rec map f seq () = match seq() with
  | Nil t -> Nil t
  | Cons (x, next) -> Cons (f x, map f next)

let rec mapt f seq () = match seq() with
  | Nil t -> Nil (f t)
  | Cons (x, next) -> Cons (x, mapt f next)

let rec filter_map f seq () = match seq() with
  | Nil t -> Nil t
  | Cons (x, next) ->
      match f x with
        | None -> filter_map f next ()
        | Some y -> Cons (y, filter_map f next)

let rec filter f seq () = match seq() with
  | Nil t -> Nil t
  | Cons (x, next) ->
      if f x
      then Cons (x, filter f next)
      else filter f next ()

let rec fold_left f acc seq ft =
  match seq () with
    | Nil t -> ft acc t
    | Cons (x, next) ->
        let acc = f acc x in
        fold_left f acc next ft

let rec iter f seq =
  match seq () with
    | Nil t -> t
    | Cons (x, next) ->
        f x;
        iter f next

let rec unfold f u () =
  match f u with
  | Either.Right t -> Nil t
  | Either.Left (x, u') -> Cons (x, unfold f u')

let is_terminated xs =
  match xs() with
  | Nil t ->
      Some t
  | Cons (_, _) ->
      None

let uncons xs =
  match xs() with
  | Cons (x, xs) ->
      Either.Left (x, xs)
  | Nil t ->
      Either.Right t



let rec length_aux accu xs ft =
  match xs() with
  | Nil t ->
      ft accu t
  | Cons (_, xs) ->
      length_aux (accu + 1) xs ft

let[@inline] length xs ft =
  length_aux 0 xs ft

let rec iteri_aux f i xs =
  match xs() with
  | Nil t ->
      t
  | Cons (x, xs) ->
      f i x;
      iteri_aux f (i+1) xs

let[@inline] iteri f xs =
  iteri_aux f 0 xs

let rec fold_lefti_aux f accu i xs ft =
  match xs() with
  | Nil t ->
      ft accu t
  | Cons (x, xs) ->
      let accu = f accu i x in
      fold_lefti_aux f accu (i+1) xs ft

let[@inline] fold_lefti f accu xs ft =
  fold_lefti_aux f accu 0 xs ft

let rec for_all p xs ft =
  match xs() with
  | Nil t ->
      ft t
  | Cons (x, xs) ->
      p x && for_all p xs ft

let rec exists p xs ft =
  match xs() with
  | Nil t ->
      ft t
  | Cons (x, xs) ->
      p x || exists p xs ft

let rec find p xs =
  match xs() with
  | Nil t ->
      Either.Right t
  | Cons (x, xs) ->
      if p x then Either.Left x else find p xs

let rec find_map f xs =
  match xs() with
  | Nil t ->
      Either.Right t
  | Cons (x, xs) ->
      match f x with
      | None ->
          find_map f xs
      | Some result ->
          Either.Left result

let rec equal eq eqt xs ys =
  match xs(), ys() with
  | Nil tx, Nil ty ->
      eqt tx ty
  | Cons (x, xs), Cons (y, ys) ->
      eq x y && equal eq eqt xs ys
  | Nil _, Cons (_, _)
  | Cons (_, _), Nil _ ->
      false

let rec compare cmp cmpt xs ys =
  match xs(), ys() with
  | Nil tx, Nil ty ->
      cmpt tx ty
  | Cons (x, xs), Cons (y, ys) ->
      let c = cmp x y in
      if c <> 0 then c else compare cmp cmpt xs ys
  | Nil _, Cons (_, _) ->
      -1
  | Cons (_, _), Nil _ ->
      +1



(* [init_aux f i j] is the sequence [f i, ..., f (j-1)]. *)

let rec init_aux f i j t () =
  if i < j then begin
    Cons (f i, init_aux f (i + 1) j t)
  end
  else
    Nil t

let init n f t =
  if n < 0 then
    invalid_arg "Seq.init"
  else
    init_aux f 0 n t

let rec flat_iterate f t () =
  append (f t) (fun t -> (fun () -> flat_iterate f t ())) ()

let rec repeat x () =
  Cons (x, repeat x)

let rec cycle xs =
  append xs (fun t -> cons t (cycle xs))

let rec forever f () =
  Cons (f(), forever f)

(* [iterate1 f x] is the sequence [f x, f (f x), ...].
   It is equivalent to [tail (iterate f x)].
   [iterate1] is used as a building block in the definition of [iterate]. *)

let rec iterate1 f x () =
  let y = f x in
  Cons (y, iterate1 f y)

(* [iterate f x] is the sequence [x, f x, ...]. *)

(* The reason why we give this slightly indirect definition of [iterate],
   as opposed to the more naive definition that may come to mind, is that
   we are careful to avoid evaluating [f x] until this function call is
   actually necessary. The naive definition (not shown here) computes the
   second argument of the sequence, [f x], when the first argument is
   requested by the user. *)

let iterate f x =
  cons x (iterate1 f x)



let rec mapi_aux f i xs () =
  match xs() with
  | Nil t ->
      Nil t
  | Cons (x, xs) ->
      Cons (f i x, mapi_aux f (i+1) xs)

let[@inline] mapi f xs =
  mapi_aux f 0 xs

(* [tail_scan f s xs] is equivalent to [tail (scan f s xs)].
   [tail_scan] is used as a building block in the definition of [scan]. *)

(* This slightly indirect definition of [scan] is meant to avoid computing
   elements too early; see the above comment about [iterate1] and [iterate]. *)

let rec tail_scan f s xs () =
  match xs() with
  | Nil t ->
      Nil t
  | Cons (x, xs) ->
      let s = f s x in
      Cons (s, tail_scan f s xs)

let scan f s xs =
  cons s (tail_scan f s xs)

(* [take] is defined in such a way that [take 0 xs] returns [empty]
   immediately, without allocating any memory. *)

let rec take_aux n xs =
  if n = 0 then
    fun () -> Nil None
  else
    fun () ->
      match xs() with
      | Nil t ->
          Nil (Some t)
       | Cons (x, xs) ->
          Cons (x, take_aux (n-1) xs)

let take n xs =
  if n < 0 then invalid_arg "Seq.take";
  take_aux n xs

(* [force_drop n xs] is equivalent to [drop n xs ()].
   [force_drop n xs] requires [n > 0].
   [force_drop] is used as a building block in the definition of [drop]. *)

let rec force_drop n xs =
  match xs() with
  | Nil t ->
      Nil t
  | Cons (_, xs) ->
      let n = n - 1 in
      if n = 0 then
        xs()
      else
        force_drop n xs

(* [drop] is defined in such a way that [drop 0 xs] returns [xs] immediately,
   without allocating any memory. *)

let drop n xs =
  if n < 0 then invalid_arg "Seq.drop"
  else if n = 0 then
    xs
  else
    fun () ->
      force_drop n xs

let rec take_while p xs () =
  match xs() with
  | Nil t ->
      Nil (Some t)
  | Cons (x, xs) ->
      if p x then Cons (x, take_while p xs) else Nil None

let rec drop_while p xs () =
  match xs() with
  | Nil t ->
      Nil t
  | Cons (x, xs) as node ->
      if p x then drop_while p xs () else node

let rec group eq xs () =
  match xs() with
  | Nil t ->
      Nil t
  | Cons (x, xs) ->
      Cons (Seq.cons x (to_seq (take_while (eq x) xs) (fun _ -> None)), group eq (drop_while (eq x) xs))

module Suspension = struct

  type 'a suspension =
    unit -> 'a

  (* Conversions. *)

  let to_lazy : 'a suspension -> 'a Lazy.t =
    Lazy.from_fun
    (* fun s -> lazy (s()) *)

  let from_lazy (s : 'a Lazy.t) : 'a suspension =
    fun () -> Lazy.force s

  (* [memoize] turns an arbitrary suspension into a persistent suspension. *)

  let memoize (s : 'a suspension) : 'a suspension =
    from_lazy (to_lazy s)

  (* [failure] is a suspension that fails when forced. *)

  let failure : _ suspension =
    fun () ->
      (* A suspension created by [once] has been forced twice. *)
      raise Seq.Forced_twice

  (* If [f] is a suspension, then [once f] is a suspension that can be forced
     at most once. If it is forced more than once, then [Forced_twice] is
     raised. *)

  let once (f : 'a suspension) : 'a suspension =
    let action = Atomic.make f in
    fun () ->
      (* Get the function currently stored in [action], and write the
         function [failure] in its place, so the next access will result
         in a call to [failure()]. *)
      let f = Atomic.exchange action failure in
      f()

end (* Suspension *)

let rec memoize xs =
  Suspension.memoize (fun () ->
    match xs() with
    | Nil t ->
        Nil t
    | Cons (x, xs) ->
        Cons (x, memoize xs)
  )

let rec once xs =
  Suspension.once (fun () ->
    match xs() with
    | Nil t ->
        Nil t
    | Cons (x, xs) ->
        Cons (x, once xs)
  )



let rec map_fst xys () =
  match xys() with
  | Nil t ->
      Nil t
  | Cons ((x, _), xys) ->
      Cons (x, map_fst xys)

let rec map_snd xys () =
  match xys() with
  | Nil t ->
      Nil t
  | Cons ((_, y), xys) ->
      Cons (y, map_snd xys)

let unzip xys =
  map_fst xys, map_snd xys

let split =
  unzip

(* [filter_map_find_left_map f xs] is equivalent to
   [filter_map Either.find_left (map f xs)]. *)

let rec filter_map_find_left_map f xs () =
  match xs() with
  | Nil t ->
      Nil t
  | Cons (x, xs) ->
      match f x with
      | Either.Left y ->
          Cons (y, filter_map_find_left_map f xs)
      | Either.Right _ ->
          filter_map_find_left_map f xs ()

let rec filter_map_find_right_map f xs () =
  match xs() with
  | Nil t ->
      Nil t
  | Cons (x, xs) ->
      match f x with
      | Either.Left _ ->
          filter_map_find_right_map f xs ()
      | Either.Right z ->
          Cons (z, filter_map_find_right_map f xs)

let partition_map f xs =
  filter_map_find_left_map f xs,
  filter_map_find_right_map f xs

let partition p xs =
  filter p xs, filter (fun x -> not (p x)) xs

let of_dispenser it =
  let rec c () =
    match it() with
    | Either.Right t ->
        Nil t
    | Either.Left x ->
        Cons (x, c)
  in
  c

let to_dispenser xs =
  let s = ref xs in
  fun () ->
    match (!s)() with
    | Nil t ->
        Either.Right t
    | Cons (x, xs) ->
        s := xs;
        Either.Left x



let rec ints i () =
  Cons (i, ints (i + 1))
