# Seqes

A library to compose the abstraction of the `Stdlib.Seq` module with monads.

The `Stdlib.Seq` module doesn't allow mixing in monads. Specifically, the
`Stdlib.Seq` module provides the following type definition:

```
type 'a t = unit -> 'a node
and +'a node =
  | Nil
  | Cons of 'a * 'a t
```

in which an `'a Seq.t` must be a simple lambda returning a node. You cannot
thread that lambda into a monad.

The `Seqes` library provides functors to generate new `Seq`-like modules,
adapted to your monad.


## Examples

Check the file `examples/seqlwt/seqlwt.ml` for an example use of most features
of Seqes. Other files in the `examples/` directory may also be of interest.


## `Seqes.Standard`

The functor `Seqes.Standard.Make1` takes a monad parameter and returns a
module with functions to traverse the `Stdlib` sequences with the monad. Typical
usage is:

```
module Seq = (* Shadow the Seq module *)
   include Stdlib.Seq (* with the Seq module *)
   module Lwt = Seqes.Standard.Make1(Lwt) (* and monad traversors *)
end
```

With this module defined you can iterate (or fold, or search, etc.) a standard
Sequence passing a function that is within the chosen monad. E.g.,

```
let cat filenames =
   filenames
   |> Seq.Lwt.iter
      (fun filename ->
         Lwt_io.with_file ~mode:input filename
            (fun inchan -> Lwt_io.(write_lines stdout (read_lines inchan))))
```


## `Seqes.Monadic.Make1`

The functor `Seqes.Monadic.Make1` takes a monad parameter and returns a module
with a brand new sequence type which uses the given monad.

E.g., consider the following functor application.

```
module Lwt_seq = Seqes.Monadic.Make1(Lwt)
```

It returns a module which contains

a. the following type definition

    ```
    type 'a t = unit -> 'a node Lwt.t
    and 'a node =
      | Nil
      | Cons of 'a * 'a t
    ```

b. all the function from the Stdlib such as

    ```
    val iter : ('a -> unit) -> 'a Lwt_seq.t -> unit Lwt_seq.t
    ```

c. a module `M` with all the functions from the Stdlib but with the monad baked
   in the traversor functions such as

    ```
    module M : sig
      val iter : ('a -> unit Lwt.t) -> 'a Lwt_seq.t -> unit Lwt_seq.t
      ...
    ```

d. functors to mix in more monads.

See `examples/seqlwt/seqlwt.ml` for an example use of `Seqes.Standard.Make1`.

## `Seqes.Standard.Make2` and `Seqes.Monadic.Make2`

If your monad has two type parameters (e.g., `('a, 'e) result`), then you can
use `Seqes.Standard.Make2` and `Seqes.Monadic.Make2` instead of the functors
mentioned above.
