(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*   Copyright 2025 Raphaël Proust                                        *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

module ListMonad = struct
  type 'a t = 'a list
  let return x = [x]
  let bind x f = List.concat_map f x
end

(** Lazy sequences in the List monad. Why not? *)
module SeqList
: sig
  include Seqes.Sigs.SEQMON1ALL
    with type 'a mon := 'a list

  module M :
    Seqes.Sigs.SEQMON1TRANSFORMERS
      with type 'a mon := 'a list
      with type 'a callermon := 'a list
      with type 'a t := 'a t

  (* The option monad can be lifted into the list monad, so we can have
     [map: ('a -> 'b option) -> 'a t -> 'b t] *)
  module O :
    Seqes.Sigs.SEQMON1TRANSFORMERS
      with type 'a mon := 'a list
      with type 'a callermon := 'a option
      with type 'a t := 'a t
end
= struct
  include Seqes.Monadic.Make1(ListMonad)
  module O = Make
    (struct
      type 'a t = 'a option
      let return x = Some x
      let bind = Option.bind
    end)
    (struct
      let bind x f = match x with None -> [] | Some x -> ListMonad.bind [x] f
    end)
end

let _ =
  SeqList.of_seq (List.to_seq [1;2;3])
  |> SeqList.map (fun x -> x + 1)
  |> SeqList.M.map (fun x -> [x; x+11; x+24])
  |> SeqList.O.map (fun x -> if x > 10 then Some (x - 10) else None)
  |> SeqList.fold_left (fun xs x -> x :: xs) []
  |> List.rev
  |> List.iter
      (fun x ->
        Format.printf "%a\n%!"
        (fun fmt xs ->
          Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt " ")
            Format.pp_print_int
            fmt
            xs)
        x)
