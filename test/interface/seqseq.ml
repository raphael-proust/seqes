(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*   Copyright 2025 Raphaël Proust                                        *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Just proving that the interface is compatible with vanilla Seq *)

module SeqSeq
: Seqes.Sigs.SEQMON1ALL (* interface from Seqes *)
    with type 'a t := 'a Stdlib.Seq.t
    with type 'a mon := 'a
= struct
  include Stdlib.Seq (* implementation from Stdlib *)
  let of_seq = Fun.id
end

let () = exit 0
