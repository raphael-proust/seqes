(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Tests Seqstd against Stdlib *)

module Make(M: TestMonads.M2)
: sig
   val tests : unit Alcotest.test_case list
end
= struct
   (* module to test: [Seqes.Standard.Make2(..)] *)
   module SeqM
   : Seqes.Sigs.SEQMON2TRAVERSORS
       with type ('a, 'e) mon := ('a, 'e) M.t
       with type ('a, 'e) callermon := ('a, 'e) M.t
       with type ('a, 'e) t := 'a Stdlib.Seq.t
   = Seqes.Standard.Make2(M)

   (* Functorised boilerplate *)
   module TestSupport
   : TestHelpers.SUPPORT2
      with module Mon = M
      with module CallerMon = M
      with type ('a, 'e) seqm = 'a Stdlib.Seq.t
   = TestHelpers.MakeSupport2
      (TestMonads.TransparentId2)
      (M)
      (M)
      (struct
         include Stdlib.Seq
         type ('a, 'e) t = 'a Stdlib.Seq.t
         let of_seq x = x
      end)

   (* Standard functor only returns traversors so that is what we test *)
   module Tests = TestHelpers.MakeTraversors2Tests(TestSupport)(SeqM)
   include Tests
end

module TestsId = Make(TestMonads.OpaqueId2)
module TestsRes = Make(TestMonads.Result)

let () =
  Alcotest.run
  "Seqes.Standard.Make2"
  [
     ("OpaqueId", TestsId.tests);
     ("Result", TestsRes.tests);
  ]

