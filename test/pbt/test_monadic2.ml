(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Tests for [Monadic.Make2].
   - We apply the functor to different monads: [OpaqueId2] and [Result].
   - We tests the top-level content of the module, the inner module [M], and the
   inner functor [Make].
   - We tests against Stdlib in every case. *)

module Make
   (M: TestMonads.M2)
   (Alt: TestMonads.M2) (* Alt is used when making transformers and traversors *)
   (Ret: TestMonads.M2) (* Ret is used when making traversors *)
: sig
   val tests : (string * unit Alcotest.test_case list) list
end
= struct

   (* The module that is being tested *)
   module SeqM = Seqes.Monadic.Make2(M)

   (* First tests for the basic core of SeqM *)
   module SupportM
   : TestHelpers.SUPPORT2
      with module Mon = M
      with module CallerMon = TestMonads.TransparentId2 (* callermon is id at top-level *)
      with type ('a, 'e) seqm = ('a, 'e) SeqM.t
   = TestHelpers.MakeSupport2
      (M)
      (M)
      (TestMonads.TransparentId2)
      (SeqM)
   module TestsM = TestHelpers.MakeAll2Tests(SupportM)(SeqM)

   (* Second tests for the module [M] of SeqM *)
   module SupportMM
   : TestHelpers.SUPPORT2
      with module Mon = M
      with module CallerMon = M
      with type ('a, 'e) seqm = ('a, 'e) SeqM.t
   = TestHelpers.MakeSupport2
     (M)
     (M)
     (M)
     (SeqM)
   module TestsMM = TestHelpers.MakeTransformers2Tests(SupportMM)(SeqM.M)

   (* Third tests for the functor [Make] of SeqM. [Alt] comes into play here.
      And so does some Glue. *)
   module SupportMMake
   : TestHelpers.SUPPORT2
      with module Mon = M
      with module CallerMon = Alt
      with type ('a, 'e) seqm = ('a, 'e) SeqM.t
   = TestHelpers.MakeSupport2
      (M)
      (M)
      (Alt)
      (SeqM)
   module SeqMMake = SeqM.Make(Alt)(TestMonads.MakeGlue2(Alt)(M)(M))
   module TestsMMake = TestHelpers.MakeTransformers2Tests(SupportMMake)(SeqMMake)

   (* Finally tests for the functor [MakeTraversors] of SeqM. Both [Alt] and
      [Ret] are used here. Glue is more sticky. *)
   module SupportMMakeTraversors
   : TestHelpers.SUPPORT2
      with module Mon = Ret
      with module CallerMon = Alt
      with type ('a, 'e) seqm = ('a, 'e) SeqM.t
   = TestHelpers.MakeSupport2
      (M)
      (Ret)
      (Alt)
      (SeqM)
   module SeqMMakeTraversors =
      SeqM.MakeTraversors
         (Alt)
         (Ret)
         (TestMonads.MakeGlue2(Alt)(Ret)(Ret))
         (TestMonads.MakeGlue2(M)(Ret)(Ret))
   module TestsMMakeTraversors =
      TestHelpers.MakeTraversors2Tests(SupportMMakeTraversors)(SeqMMakeTraversors)

      (* The tests have partial names which are completed afterwards (see [name]
         below). *)
   let tests = [
      ("", TestsM.tests);
      (".M", TestsMM.tests);
      (".Make", TestsMMake.tests);
      (".MakeTraversors", TestsMMakeTraversors.tests);
   ]
end

let name n xs =
   List.map (fun (s, t) -> (n ^ s, t)) xs

(* Different tests for different combinations of monads. More than necessary. *)
module TestsIdsIds = Make(TestMonads.OpaqueId2)(TestMonads.OpaqueId2)(TestMonads.OpaqueId2)
module TestsIdsRes = Make(TestMonads.OpaqueId2)(TestMonads.Result)(TestMonads.Result)
module TestsResRes = Make(TestMonads.Result)(TestMonads.Result)(TestMonads.Result)

let () =
   Alcotest.run
   "Seqes.Monadic.Make2"
   (name "IdsIds" (TestsIdsIds.tests)
   @ name "IdsRes" (TestsIdsRes.tests)
   @ name "ResRes" (TestsResRes.tests))
