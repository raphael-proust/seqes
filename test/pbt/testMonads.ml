(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*   Copyright 2025 Raphaël Proust                                        *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* For tests we need "breakable" monad because the test runners need to inspect
   the values inside *)

module type M1 = sig
   include Seqes.Sigs.MONAD1
   val break : 'a t -> 'a
end
module type M2 = sig
   include Seqes.Sigs.MONAD2
   val break : ('a, 'e) t -> 'a
end

(* Some M1 monads *)

module TransparentId1
: M1 with type 'a t = 'a
= struct
   type 'a t = 'a
   let return x = x
   let bind x f = f x
   let break x = x
end

module OpaqueId1
: M1
= TransparentId1

module Option
: M1
= struct
   type 'a t = 'a option
   let return x = Some x
   let bind = Option.bind
   let break = Option.get (* can't fail, None doesn't exists *)
end

(* Some M2 monads *)

module TransparentId2
: M2 with type ('a, 'e) t = 'a
= struct
   type ('a, 'e) t = 'a
   let return x = x
   let bind x f = f x
   let break x = x
end

module OpaqueId2
: M2
= TransparentId2

module Result
: M2
= struct
   type ('a, 'e) t = ('a, 'e) result
   let return x = Ok x
   let bind = Result.bind
   let break = Result.get_ok (* can't fail, Error doesn't exists *)
end

(* Glue is how seqes mixes monads together, with breakable monads we can
   generate the glue easily *)

module MakeGlue1
   (X: M1)
   (F: M1)
   (Ret: M1)
: Seqes.Sigs.GLUE1
   with type 'a x := 'a X.t
   with type 'a f := 'a F.t
   with type 'a ret := 'a Ret.t
= struct
   let bind x f = Ret.return (F.break (f (X.break x)))
end

module Opaque1Glue
: Seqes.Sigs.GLUE1
   with type 'a x := 'a OpaqueId1.t
   with type 'a f := 'a OpaqueId1.t
   with type 'a ret := 'a OpaqueId1.t
= MakeGlue1(OpaqueId1)(OpaqueId1)(OpaqueId1)

module OptionGlue
: Seqes.Sigs.GLUE1
   with type 'a x := 'a Option.t
   with type 'a f := 'a Option.t
   with type 'a ret := 'a Option.t
= MakeGlue1(Option)(Option)(Option)

module MakeGlue2
   (X: M2)
   (F: M2)
   (Ret: M2)
: Seqes.Sigs.GLUE2
   with type ('a, 'e) x := ('a, 'e) X.t
   with type ('a, 'e) f := ('a, 'e) F.t
   with type ('a, 'e) ret := ('a, 'e) Ret.t
= struct
   let bind x f = Ret.return (F.break (f (X.break x)))
end

module Twoify
   (M: M1)
: M2
   with type ('a, 'e) t = 'a M.t
= struct
   include M
   type nonrec ('a, 'e) t = 'a t
end
