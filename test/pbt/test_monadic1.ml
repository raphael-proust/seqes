(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*   Copyright 2025 Raphaël Proust                                        *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Tests for [Monadic.Make1].
   - We apply the functor to different monads: [OpaqueId1] and [Option].
   - We tests the top-level content of the module, the inner module [M], and the
   inner functor [Make].
   - We tests against Stdlib in every case. *)

module Make
   (M: TestMonads.M1)
   (Alt: TestMonads.M1) (* Alt is used when making transformers and traversors *)
   (Ret: TestMonads.M1) (* Ret is used when making traversors *)
   (Alt2: TestMonads.M2) (* Alt2 is used when making 2-parameter traversors *)
   (Ret2: TestMonads.M2) (* Ret2 is used when making 2-parameter traversors *)
: sig
   val tests : (string * unit Alcotest.test_case list) list
end
= struct

   (* The module that is being tested *)
   module SeqM = Seqes.Monadic.Make1(M)

   (* First tests for the basic core of SeqM *)
   module SupportM
   : TestHelpers.SUPPORT1
      with module Mon = M
      with module CallerMon = TestMonads.TransparentId1 (* callermon is id at top-level *)
      with type 'a seqm = 'a SeqM.t
   = TestHelpers.MakeSupport1
      (M)
      (M)
      (TestMonads.TransparentId1)
      (SeqM)
   module TestsM = TestHelpers.MakeAll1Tests(SupportM)(SeqM)

   (* Second tests for the module [M] of SeqM *)
   module SupportMM
   : TestHelpers.SUPPORT1
      with module Mon = M
      with module CallerMon = M
      with type 'a seqm = 'a SeqM.t
   = TestHelpers.MakeSupport1
     (M)
     (M)
     (M)
     (SeqM)
   module TestsMM = TestHelpers.MakeTransformers1Tests(SupportMM)(SeqM.M)

   (* Third tests for the functor [Make] of SeqM. [Alt] comes into play here.
      And so does some Glue. *)
   module SupportMMake
   : TestHelpers.SUPPORT1
      with module Mon = M
      with module CallerMon = Alt
      with type 'a seqm = 'a SeqM.t
   = TestHelpers.MakeSupport1
      (M)
      (M)
      (Alt)
      (SeqM)
   module SeqMMake = SeqM.Make(Alt)(TestMonads.MakeGlue1(Alt)(M)(M))
   module TestsMMake = TestHelpers.MakeTransformers1Tests(SupportMMake)(SeqMMake)

   (* Fourth tests for the functor [MakeTraversors] of SeqM. Both [Alt] and
      [Ret] are used here. Glue is more sticky. *)
   module SupportMMakeTraversors
   : TestHelpers.SUPPORT1
      with module Mon = Ret
      with module CallerMon = Alt
      with type 'a seqm = 'a SeqM.t
   = TestHelpers.MakeSupport1
      (M)
      (Ret)
      (Alt)
      (SeqM)
   module SeqMMakeTraversors =
      SeqM.MakeTraversors
         (Alt)
         (Ret)
         (TestMonads.MakeGlue1(Alt)(Ret)(Ret))
         (TestMonads.MakeGlue1(M)(Ret)(Ret))
   module TestsMMakeTraversors =
      TestHelpers.MakeTraversors1Tests(SupportMMakeTraversors)(SeqMMakeTraversors)

   (* Fifth tests for the functor [MakeTraversors2] of SeqM. Both [Alt2] and
      [Ret2] are used here. Glue is more sticky. *)
   module SupportMMakeTraversors2
   : TestHelpers.SUPPORT2
      with module Mon = Ret2
      with module CallerMon = Alt2
      with type ('a, 'e) seqm = 'a SeqM.t
   = TestHelpers.MakeSupport2
      (TestMonads.Twoify(M))
      (Ret2)
      (Alt2)
      (struct
         include SeqM
         type nonrec ('a, 'e) t = 'a t
      end)
   module SeqMMakeTraversors2 =
      SeqM.MakeTraversors2
         (Alt2)
         (Ret2)
         (TestMonads.MakeGlue2(Alt2)(Ret2)(Ret2))
         (TestMonads.MakeGlue2(TestMonads.Twoify(M))(Ret2)(Ret2))
   module TestsMMakeTraversors2 =
      TestHelpers.MakeTraversors2Tests(SupportMMakeTraversors2)(SeqMMakeTraversors2)

      (* The tests have partial names which are completed afterwards (see [name]
         below). *)
   let tests = [
      ("", TestsM.tests);
      (".M", TestsMM.tests);
      (".Make", TestsMMake.tests);
      (".MakeTraversors", TestsMMakeTraversors.tests);
      (".MakeTraversors2", TestsMMakeTraversors2.tests);
   ]
end

let name n xs =
   List.map (fun (s, t) -> (n ^ s, t)) xs

(* Different tests for different combinations of monads. More than necessary. *)
module TestsIds =
   Make
      (TestMonads.OpaqueId1) (* M *)
      (TestMonads.OpaqueId1) (* Alt *)
      (TestMonads.OpaqueId1) (* Ret *)
      (TestMonads.OpaqueId2) (* Alt2 *)
      (TestMonads.OpaqueId2) (* Ret2 *)
module TestsIdsOptRes =
   Make
      (TestMonads.OpaqueId1) (* M *)
      (TestMonads.Option) (* Alt *)
      (TestMonads.Option) (* Ret *)
      (TestMonads.Result) (* Alt2 *)
      (TestMonads.Result) (* Ret2 *)
module TestsOptOptRes =
   Make
      (TestMonads.Option) (* M *)
      (TestMonads.Option) (* Alt *)
      (TestMonads.Option) (* Ret *)
      (TestMonads.Result) (* Alt2 *)
      (TestMonads.Result) (* Ret2 *)

let () =
   Alcotest.run
   "Seqes.Monadic.Make1"
   (name "Ids" (TestsIds.tests)
   @ name "IdsOptRes" (TestsIdsOptRes.tests)
   @ name "OptOptRes" (TestsOptOptRes.tests))
