(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*   Copyright 2025 Raphaël Proust                                        *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Support module for tests relating to 1-parameter monads *)

module type SUPPORT1
= sig

   (* The Monad that the sequences are parametrised over *)
   module Mon : Seqes.Sigs.MONAD1

   (* The Monad that the sequences traversors are called with *)
   module CallerMon : Seqes.Sigs.MONAD1

   (* The type of the monadic sequence *)
   type 'a seqm

   (* The type of values that describe types. Specifically they describe the
      interface of different parts of Seqes. See [DSL] below for constructors *)
   type (_, _, _) ty
   and (_, _, _, _, _, _) params =
   | [] : ('vr, 'vr, 'mr, 'mr, 'wr, 'wr) params
   | ( :: ) : ('vp, 'mp, 'wp) ty * ('vk, 'vr, 'mk, 'mr, 'wk, 'wr) params ->
       ('vp -> 'vk, 'vr, 'mp -> 'mk, 'mr, 'wp -> 'wk, 'wr) params

   (* The eDSL to describe functions *)
   module DSL : sig
      val unit : (unit, unit, unit) ty
      val data : (char, char, char) ty (* used to describe data: the elements of the sequences *)
      val int : (int, int, int) ty
      val nat : (int, int, int) ty
      val bool : (bool, bool, bool) ty
      val ( * ) : ('a, 'b, 'c) ty -> ('u, 'v, 'w) ty -> ('a * 'u, 'b * 'v, 'c * 'w) ty
      val option : ('a, 'b, 'c) ty -> ('a option, 'b option, 'c option) ty
      val either : ('a, 'b, 'c) ty -> ('u, 'v, 'w) ty -> (('a, 'u) Either.t, ('b, 'v) Either.t, ('c, 'w) Either.t) ty
      val seq : ('a, 'b, 'c) ty -> ('a Seq.t, 'b seqm, 'b) ty
      val monad : ('a, 'b, 'c) ty -> ('a, 'b Mon.t, 'b) ty
      val callermonad : ('a, 'b, 'c) ty -> ('a, 'b CallerMon.t, 'b) ty
      val ( @-> ) : ('a, 'b, 'c, 'd CallerMon.t, 'e, 'd) params -> ('b, 'd, 'f) ty -> ('a, 'c, 'd) ty
      val ( ->> ) : ('a, 'b, 'c, 'd Mon.t, 'e, 'd) params -> ('b, 'd, 'f) ty -> ('a, 'c, 'd) ty
      val ( --> ) : ('a, 'b, 'c, 'd, 'e, 'f) params -> ('b, 'd, 'f) ty -> ('a, 'b, 'c, 'd, 'e, 'f) params * ('b, 'd, 'f) ty
   end

   (* A function to convert a type description into a test *)
   val test_of_ty : string -> ('a, 'b, 'c, 'd, 'e, 'f) params * ('b, 'd, 'f) ty -> 'a -> 'c -> unit Alcotest.test_case

end

(* Generates support modules from monads and such

   [SeqMon]: the monad the sequence goes over
   [Mon]: the monad the functions return in
   [CallerMon]: the monad the functions-arguments return in
   [SeqM]: the module being tested *)
module MakeSupport1
   (SeqMon : TestMonads.M1)
   (Mon : TestMonads.M1)
   (CallerMon : TestMonads.M1)
   (SeqM : sig (* only use this part of the monadic seq *)
      type 'a t
      val of_seq : 'a Seq.t -> 'a t
      val map : ('a -> 'b) -> 'a t -> 'b t
      val uncons : 'a t -> ('a * 'a t) option SeqMon.t
   end)
: SUPPORT1
   with module Mon = Mon
   with module CallerMon = CallerMon
   with type 'a seqm = 'a SeqM.t
= struct
   module Mon = Mon
   module CallerMon = CallerMon
   type 'a seqm = 'a SeqM.t

   (* QCheck2 generator for sequences *)
   module GenSeq
   : sig
     val small : ('a * 'b) QCheck2.Gen.t -> ('a Seq.t * 'b SeqM.t) QCheck2.Gen.t
   end
   = struct

      let small g =
         QCheck2.Gen.map
            (fun vml ->
               let vms = List.to_seq vml in
               let vs, ms = Seq.unzip vms in
               (vs, SeqM.of_seq ms))
            (QCheck2.Gen.oneof [
               QCheck2.Gen.pure [];
               QCheck2.Gen.small_list g;
            ])

   end


   (* The [ty] type is for describing items of the Seq interface. Each variant of
      [ty] corresponds to a type (e.g., [Unit] is for [unit]), a type variable
      (e.g., [Char] is used for the element of the sequences), or a type
      constructor (e.g., [Option] is for [option]).

      The two first parameters are for plain (a.k.a., [v] for value) and monadic
      (a.k.a. [m]) variants. We need both descriptions because we compare the
      monadic functions of the [SeqM] functor parameter to the plain functions
      of [Stdlib.Seq].

      The third parameter is for injectivity: we give the type checker the
      parameter of the monadic type to hold, so that it can't be erased by some
      later substitution.

      The lambdas are described as [Lambda (params, return)] where [params] encodes
      all the types of all the parameters of the lambda and [return] encodes the
      type of the return value of the lambda. The first type parameter of [params]
      is the full lambda type (with the [->]), the second is the return type, the
      third is the [->] but for the monad side of things, the last is the return
      but for the monad side of things. E.g., [Lambda ([Char; Char], Bool)] is for
      a lambda of the type [(char -> char -> bool)]. *)
   type (_, _, _) ty =
     | Unit : (unit, unit, unit) ty
     | Char : (char, char, char) ty (* use for the elements of sequences, see [data] below *)
     | Int : (int, int, int) ty
     | Nat : (int, int, int) ty (* positive integers for indexes and lengths *)
     | Bool : (bool, bool, bool) ty
     | Tup2 : ('va, 'ma, 'wa) ty * ('vb, 'mb, 'wb) ty -> (('va * 'vb), ('ma * 'mb), ('wa * 'wb)) ty
     | Option : ('v, 'm, 'w) ty -> ('v option, 'm option, 'w option) ty
     | Either : ('va, 'ma, 'wa) ty * ('vb, 'mb, 'wb) ty -> (('va, 'vb) Either.t, ('ma, 'mb) Either.t, ('wa, 'wb) Either.t) ty
     | Lambda : ('vk, 'vr, 'mk, 'mr, 'wk, 'wr) params * ('vr, 'mr, 'wr) ty -> ('vk, 'mk, 'wr) ty
     | Seq : ('va, 'ma, _) ty -> ('va Seq.t, 'ma SeqM.t, 'ma) ty
     | Monad : ('va, 'ma, _) ty -> ('va, 'ma Mon.t, 'ma) ty
     | CallerMonad : ('va, 'ma, _) ty -> ('va, 'ma CallerMon.t, 'ma) ty
   and (_, _, _, _, _, _) params =
     | [] : ('vr, 'vr, 'mr, 'mr, 'wr, 'wr) params
     | ( :: ) : ('vp, 'mp, 'wp) ty * ('vk, 'vr, 'mk, 'mr, 'wk, 'wr) params ->
         ('vp -> 'vk, 'vr, 'mp -> 'mk, 'mr, 'wp -> 'wk, 'wr) params

   (* Helper syntax for describing items of the Seq interface *)
   module DSL = struct
      let unit = Unit
      let data = Char (* used to describe data: the elements of the sequences *)
      let int = Int
      let nat = Nat
      let bool = Bool
      let ( * ) a b = Tup2 (a, b)
      let option a = Option a
      let either a b = Either (a, b)
      let seq a = Seq a
      let monad a = Monad a
      let callermonad a = CallerMonad a
      let ( @-> ) p r = Lambda (p, CallerMonad r) (* used for inner lambda, hence caller *)
      let ( ->> ) p r = Lambda (p, Monad r) (* used for to_dispenser only *)
      let ( --> ) p r = (* used for top-level lambdas *)
         let _ = Lambda (p, r) in (* this imposes some restrictions on the types *)
         (p, r) (* we don't want it wrapped in a [Lambda] constructor for practical reasons *)
   end

   (* makes the monad version of a plain value (based on the the ty descriptor) *)
   let rec monadify_of_ty : type a b c. (a, b, c) ty -> a -> b = function
     | Unit -> fun () -> ()
     | Char -> fun a -> a
     | Int -> fun a -> a
     | Nat -> fun a -> a
     | Bool -> fun a -> a
     | Tup2 (tyx, tyy) -> fun (x, y) ->
         let monadifier_x = monadify_of_ty tyx in
         let monadifier_y = monadify_of_ty tyy in
         (monadifier_x x, monadifier_y y)
     | Option ty -> (function
         | Some a ->
             let monadifier = monadify_of_ty ty in
             Some (monadifier a)
         | None -> None
     )
     | Either (tyl, tyr) -> (function
         | Either.Left l ->
             let monadifier = monadify_of_ty tyl in
             Either.Left (monadifier l)
         | Either.Right r ->
             let monadifier = monadify_of_ty tyr in
             Either.Right (monadifier r)
     )
     | Lambda _ -> invalid_arg "monadifier_of_ty.Lambda: unimplemented"
     | Seq ty -> fun s ->
         let monadifier = monadify_of_ty ty in
         SeqM.of_seq (Seq.map monadifier s)
     | Monad ty -> (fun v ->
         let monadifier = monadify_of_ty ty in
         Mon.return (monadifier v))
     | CallerMonad ty -> (fun v ->
         let monadifier = monadify_of_ty ty in
         CallerMon.return (monadifier v))

   (* makes the plain version of a monad value (based on the the ty descriptor) *)
   let rec unmonadify_of_ty : type a b c. (a, b, c) ty -> b -> a = function
     | Unit -> fun () -> ()
     | Char -> fun b -> b
     | Int -> fun b -> b
     | Nat -> fun b -> b
     | Bool -> fun b -> b
     | Tup2 (tyx, tyy) -> fun (x, y) ->
         let unmonadifier_x = unmonadify_of_ty tyx in
         let unmonadifier_y = unmonadify_of_ty tyy in
         (unmonadifier_x x, unmonadifier_y y)
     | Option ty -> (function
         | Some a ->
             let unmonadifier = unmonadify_of_ty ty in
             Some (unmonadifier a)
         | None -> None
     )
     | Either (tyl, tyr) -> (function
         | Either.Left l ->
             let unmonadifier = unmonadify_of_ty tyl in
             Either.Left (unmonadifier l)
         | Either.Right r ->
             let unmonadifier = unmonadify_of_ty tyr in
             Either.Right (unmonadifier r)
     )
     | Lambda _ -> invalid_arg "unmonadifier_of_ty.Lambda: unimplemented"
     | Seq ty -> fun s ->
         let unmonadifier = unmonadify_of_ty ty in
         seq_of_seqm (SeqM.map unmonadifier s)
     | Monad ty -> (fun v ->
         let unmonadifier = unmonadify_of_ty ty in
         unmonadifier (Mon.break v))
     | CallerMonad ty -> (fun v ->
         let unmonadifier = unmonadify_of_ty ty in
         unmonadifier (CallerMon.break v))
   and seq_of_seqm : type a. a SeqM.t -> a Seq.t = fun seqm ->
      fun () ->
         match SeqMon.break (SeqM.uncons seqm) with
               | None -> Seq.Nil
               | Some (v, seqm) -> Seq.Cons (v, (seq_of_seqm seqm))


   let rec eq_seq_seqm n eq seq seqm =
     n < 0 || begin
     match seq (), SeqMon.break (SeqM.uncons seqm) with
     | Stdlib.Seq.Nil, None -> true
     | Stdlib.Seq.Cons (v, seq), Some (m, seqm) ->
         eq v m && eq_seq_seqm (n-1) eq seq seqm
     | _ -> false
     end

   let eq_dispenser_dispenserm n eq dis dism =
     let rec go n =
       n < 0 || begin
       match dis (), Mon.break (dism ()) with
       | None, None -> true
       | Some v, Some m -> eq v m && go (n-1)
       | _ -> false
       end
     in
     go n

   (* compares a plain and monadic values described by a ty; note that sequences
      are potentially infinite so we limit sequence comparison to the first 100
      elements *)
   let rec eq_of_ty : type a b c. (a, b, c) ty -> (a -> b -> bool) = function
     | Unit -> fun () () -> true
     | Char -> fun a b -> a = b
     | Int -> fun a b -> a = b
     | Nat -> fun a b -> a = b
     | Bool -> fun a b -> a = b
     | Tup2 (tyx, tyy) -> fun (xa, ya) (xb, yb) -> eq_of_ty tyx xa xb && eq_of_ty tyy ya yb
     | Option ty -> (fun a b -> match a, b with
         | Some a, Some b -> eq_of_ty ty a b
         | None, None -> true
         | Some _, None -> false
         | None, Some _ -> false
     )
     | Either (tyl, tyr) -> (fun a b ->
         match a, b with
         | Either.Left a, Either.Left b ->
             let eql = eq_of_ty tyl in
             eql a b
         | Either.Right a, Either.Right b ->
             let eqr = eq_of_ty tyr in
             eqr a b
         | _ -> false
     )
     | Lambda ([Unit], Monad (Option Char)) -> (* special case needed for comparing [to_dispenser] *)
         fun (a : unit -> char option) (b : unit -> char option Mon.t) ->
           eq_dispenser_dispenserm 100 Char.equal a b
     | Lambda _ -> invalid_arg "eq_of_ty.Lambda: impossible"
     | Seq ty -> (fun sa sb -> eq_seq_seqm 100 (eq_of_ty ty) sa sb)
     | Monad ty -> (fun v m -> (eq_of_ty ty) v (Mon.break m))
     | CallerMonad ty -> (fun v m -> (eq_of_ty ty) v (CallerMon.break m))

   (* in order to generate lambdas, we need observables *)
   let rec observable_of_ty : type a b c. (a, b, c) ty -> a QCheck2.Observable.t = function
     | Unit -> QCheck2.Observable.unit
     | Char -> QCheck2.Observable.char
     | Int -> QCheck2.Observable.int
     | Nat -> QCheck2.Observable.int
     | Bool -> QCheck2.Observable.bool
     | Tup2 (tyx, tyy) ->
         let ox = observable_of_ty tyx in
         let oy = observable_of_ty tyy in
         QCheck2.Observable.pair ox oy
     | Option ty ->
         let o = observable_of_ty ty in
         QCheck2.Observable.option o
     | Either _ ->
         (* never appears inside another lambda so we don't need it *)
         invalid_arg "observable_of_ty.Either: unimplemented"
     | Lambda _ ->
         (* never appears inside another lambda so we don't need it *)
         invalid_arg "observable_of_ty.Lambda: unimplemented"
     | Seq ty ->
         let o = observable_of_ty ty in
         QCheck2.Observable.map List.of_seq (QCheck2.Observable.list o)
     | Monad ty -> observable_of_ty ty
     | CallerMonad ty -> observable_of_ty ty

   (* for showing test results and such, we need to be able to pretty-print values *)
   let rec to_string_of_ty
     : type v m w. (v, m, w) ty -> (v QCheck2.Print.t * m QCheck2.Print.t)
     = fun ty -> match ty with
     | Unit -> QCheck2.Print.unit, QCheck2.Print.unit
     | Char -> QCheck2.Print.char, QCheck2.Print.char
     | Int -> QCheck2.Print.int, QCheck2.Print.int
     | Nat -> QCheck2.Print.int, QCheck2.Print.int
     | Bool -> QCheck2.Print.bool, QCheck2.Print.bool
     | Tup2 (tyx, tyy) ->
         let (pvx, pmx) = to_string_of_ty tyx in
         let (pvy, pmy) = to_string_of_ty tyy in
         ( (fun (x, y) -> Format.asprintf "(%s,%s)" (pvx x) (pvy y)) ,
           (fun (x, y) -> Format.asprintf "(%s,%s)" (pmx x) (pmy y)) )
     | Option ty ->
         let (pvx, pmx) = to_string_of_ty ty in
         ( (function
             | None -> "None"
             | Some x -> Format.asprintf "Some (%s)" (pvx x)) ,
           (function
             | None -> "None"
             | Some x -> Format.asprintf "Some (%s)" (pmx x)) )
     | Either (tya, tyb) ->
         let (pva, pma) = to_string_of_ty tya in
         let (pvb, pmb) = to_string_of_ty tyb in
         ( (function
             | Either.Left a -> Format.asprintf "Left (%s)" (pva a)
             | Either.Right b -> Format.asprintf "Right (%s)" (pvb b)) ,
           (function
             | Either.Left a -> Format.asprintf "Left (%s)" (pma a)
             | Either.Right b -> Format.asprintf "Right (%s)" (pmb b)) )
     | Lambda (_, _) ->
         ( (fun _ -> "\\") ,
           (fun _ -> "\\") )
     | Seq ty ->
         let (pvx, pmx) = to_string_of_ty ty in
         ( (fun s ->
             Format.asprintf "Seq (%s)"
               (String.concat " " (List.map pvx (List.of_seq s)))) ,
           (fun s ->
             Format.asprintf "Seq (%s)"
               (String.concat " " (List.map pmx (List.of_seq (seq_of_seqm s))))))
     | Monad ty ->
         let (pvx, pmx) = to_string_of_ty ty in
         ( pvx ,
           (fun m -> Format.asprintf "Monad (%s)" (pmx (Mon.break m))) )
     | CallerMonad ty ->
         let (pvx, pmx) = to_string_of_ty ty in
         ( pvx ,
           (fun m -> Format.asprintf "CallerMonad (%s)" (pmx (CallerMon.break m))) )

   (* QCheck2 generators, derived from [ty] descriptions. *)
   let rec gen_of_ty
     : type v m w. (v, m, w) ty -> (v * m) QCheck2.Gen.t
     = fun ty -> match ty with
     | Unit -> QCheck2.Gen.map (fun () -> ((), ())) QCheck2.Gen.unit
     | Char -> QCheck2.Gen.map (fun x -> (x, x)) QCheck2.Gen.printable
     | Int -> QCheck2.Gen.map (fun x -> (x, x)) QCheck2.Gen.int
     | Nat -> QCheck2.Gen.map (fun x -> (x, x)) QCheck2.Gen.nat
     | Bool -> QCheck2.Gen.map (fun x -> (x, x)) QCheck2.Gen.bool
     | Tup2 (tyx, tyy) ->
         let gx = gen_of_ty tyx in
         let gy = gen_of_ty tyy in
         QCheck2.Gen.map2 (fun (vx, mx) (vy, my) -> ((vx, vy), (mx, my))) gx gy
     | Option ty ->
         QCheck2.Gen.oneof [
           QCheck2.Gen.return (None, None);
           QCheck2.Gen.map (fun (v, m) -> (Some v, Some m)) (gen_of_ty ty);
         ]
     | Either (tya, tyb) ->
         let ga = gen_of_ty tya in
         let gb = gen_of_ty tyb in
         QCheck2.Gen.oneof [
           QCheck2.Gen.map (fun (va, ma) -> Either.Left va, Either.Left ma) ga ;
           QCheck2.Gen.map (fun (vb, mb) -> Either.Right vb, Either.Right mb) gb ;
         ]
     | Lambda (params, tyr) ->
         gen_of_params params tyr
     | Seq ty ->
         let g = gen_of_ty ty in
         GenSeq.small g
     | Monad ty ->
         let g = gen_of_ty ty in
         QCheck2.Gen.map (fun (v, m) -> (v, Mon.return m)) g
     | CallerMonad ty ->
         let g = gen_of_ty ty in
         QCheck2.Gen.map (fun (v, m) -> (v, CallerMon.return m)) g
   and gen_of_params
     : type vk vr mk mr wk wr. (vk, vr, mk, mr, wk, wr) params -> (vr, mr, wr) ty -> (vk * mk) QCheck2.Gen.t
     = fun params tyr ->
       match params with
       | [] ->
           (* zero-parameters functions don't exist *)
           assert false
       | [tya] ->
           let gvr = QCheck2.Gen.map fst @@ gen_of_ty tyr in
           let aov = observable_of_ty tya in
           let fg = QCheck2.fun1 aov gvr in
           QCheck2.Gen.map (fun (QCheck2.Fun (_, f)) ->
             (* The two functions need to be identical (up to the monad), so we
                just generate a single vanilla one ([f]) and make the monadic
                variant by wrapping it with the monadification and
                unmonadification functions. *)
             let unmonadifier_a = unmonadify_of_ty tya in
             let monadifier_r = monadify_of_ty tyr in
             let fm a = monadifier_r (f (unmonadifier_a a)) in
             (f, fm)
           ) fg
       | [tya; tyb] ->
           let gvr = QCheck2.Gen.map fst @@ gen_of_ty tyr in
           let aov = observable_of_ty tya in
           let bov = observable_of_ty tyb in
           let fg = QCheck2.fun2 aov bov gvr in
           QCheck2.Gen.map (fun (QCheck2.Fun (_, f)) ->
             let unmonadifier_a = unmonadify_of_ty tya in
             let unmonadifier_b = unmonadify_of_ty tyb in
             let monadifier_r = monadify_of_ty tyr in
             let fm a b = monadifier_r (f (unmonadifier_a a) (unmonadifier_b b)) in
             (f, fm)
           ) fg
       | [tya; tyb; tyc] ->
           let gvr = QCheck2.Gen.map fst @@ gen_of_ty tyr in
           let aov = observable_of_ty tya in
           let bov = observable_of_ty tyb in
           let cov = observable_of_ty tyc in
           let fg = QCheck2.fun3 aov bov cov gvr in
           QCheck2.Gen.map (fun (QCheck2.Fun (_, f)) ->
             let unmonadifier_a = unmonadify_of_ty tya in
             let unmonadifier_b = unmonadify_of_ty tyb in
             let unmonadifier_c = unmonadify_of_ty tyc in
             let monadifier_r = monadify_of_ty tyr in
             let fm a b c = monadifier_r (f (unmonadifier_a a) (unmonadifier_b b) (unmonadifier_c c)) in
             (f, fm)
           ) fg
       | [tya; tyb; tyc; tyd] ->
           let gvr = QCheck2.Gen.map fst @@ gen_of_ty tyr in
           let aov = observable_of_ty tya in
           let bov = observable_of_ty tyb in
           let cov = observable_of_ty tyc in
           let dov = observable_of_ty tyd in
           let fg = QCheck2.fun4 aov bov cov dov gvr in
           QCheck2.Gen.map (fun (QCheck2.Fun (_, f)) ->
             let unmonadifier_a = unmonadify_of_ty tya in
             let unmonadifier_b = unmonadify_of_ty tyb in
             let unmonadifier_c = unmonadify_of_ty tyc in
             let unmonadifier_d = unmonadify_of_ty tyd in
             let monadifier_r = monadify_of_ty tyr in
             let fm a b c d = monadifier_r (f (unmonadifier_a a) (unmonadifier_b b) (unmonadifier_c c) (unmonadifier_d d)) in
             (f, fm)
           ) fg
       | _ ->
           (* the maximum arity of lambdas in the Seq interface is 4 so we don't
              need more *)
           invalid_arg "gen_of_params._: unimplemented arity"

   let test_of_ty
   : type vk vr mk mr wk wr. string -> (vk, vr, mk, mr, wk, wr) params -> (vr, mr, wr) ty ->
     vk -> mk -> QCheck2.Test.t
   = fun name params tyr vf mf ->
     match params with
     | [] ->
         (* there are no lambdas with zero parameters *)
         assert false
     | [tya] ->
         QCheck2.Test.make
           ~name
           ~print:(fun (va, ma) ->
             let (pv, pm) = to_string_of_ty tya in
             Format.asprintf "{V: %s; M: %s}"
               (pv va)
               (pm ma)
           )
           (gen_of_ty tya)
           (fun (va, ma) ->
             (eq_of_ty tyr)
               (vf va)
               (mf ma))
     | [tya; tyb] ->
         let ag = gen_of_ty tya in
         let bg = gen_of_ty tyb in
         QCheck2.Test.make
           ~name
           ~print:(fun ((va, ma), (vb, mb)) ->
             let (pav, pam) = to_string_of_ty tya in
             let (pbv, pbm) = to_string_of_ty tyb in
             Format.asprintf "{V: (%s, %s); M: (%s, %s)}"
               (pav va) (pbv vb)
               (pam ma) (pbm mb)
           )
           (QCheck2.Gen.tup2 ag bg)
           (fun ((va, ma), (vb, mb)) ->
             (eq_of_ty tyr)
               (vf va vb)
               (mf ma mb))
     | [tya; tyb; tyc] ->
         let ag = gen_of_ty tya in
         let bg = gen_of_ty tyb in
         let cg = gen_of_ty tyc in
         QCheck2.Test.make
           ~name
           ~print:(fun ((va, ma), (vb, mb), (vc, mc)) ->
             let (pav, pam) = to_string_of_ty tya in
             let (pbv, pbm) = to_string_of_ty tyb in
             let (pcv, pcm) = to_string_of_ty tyc in
             Format.asprintf "{V: (%s, %s, %s); M: (%s, %s, %s)}"
               (pav va) (pbv vb) (pcv vc)
               (pam ma) (pbm mb) (pcm mc)
           )
           (QCheck2.Gen.tup3 ag bg cg)
           (fun ((va, ma), (vb, mb), (vc, mc)) ->
             (eq_of_ty tyr)
               (vf va vb vc)
               (mf ma mb mc))
     | [tya; tyb; tyc; tyd] ->
         let ag = gen_of_ty tya in
         let bg = gen_of_ty tyb in
         let cg = gen_of_ty tyc in
         let dg = gen_of_ty tyd in
         QCheck2.Test.make
           ~name
           (QCheck2.Gen.tup4 ag bg cg dg)
           (fun ((va, ma), (vb, mb), (vc, mc), (vd, md)) ->
             (eq_of_ty tyr)
               (vf va vb vc vd)
               (mf ma mb mc md))
     | _ ->
         (* the maximum arity of Seq interface item is 4 so we don't need more *)
         invalid_arg "test_of_ty._: unimplemented arity"

   let test_of_ty name (params, tyr) vf mf =
     QCheck_alcotest.to_alcotest @@ test_of_ty name params tyr vf mf

end

module MakeTraversors1Tests
   (Support : SUPPORT1)
   (SeqM
: Seqes.Sigs.SEQMON1TRAVERSORS
    with type 'a mon := 'a Support.Mon.t
    with type 'a callermon := 'a Support.CallerMon.t
    with type 'a t := 'a Support.seqm)
: sig
   val tests : unit Alcotest.test_case list
end
= struct

   open Support

   let test_equal =
     test_of_ty "equal"
       DSL.([[data; data] @-> bool; seq data; seq data] --> monad bool)
       Seq.equal
       SeqM.equal
   let test_compare =
     test_of_ty "compare"
       DSL.([[data; data] @-> int; seq data; seq data] --> monad int)
       Seq.compare
       SeqM.compare
   let test_iter =
     test_of_ty "iter"
       DSL.([ [data; data] @-> data; data; seq data ] --> monad data)
       (fun f init s ->
         let r = ref init in
         Seq.iter (fun x -> r := f !r x) s;
         !r)
       (fun f init s ->
         (* Because we want to observe side-effects, we have to do some extra
            legwork around the monad *)
         let r = ref init in
         Mon.bind
           (SeqM.iter
              (fun x ->
                 CallerMon.bind
                   (f !r x)
                   (fun rr -> r := rr; CallerMon.return ()))
              s)
           (fun () -> Mon.return !r))
   let test_fold_left =
     test_of_ty "fold_left"
       DSL.([ [data; data] @-> data; data; seq data ] --> monad data)
       Seq.fold_left
       SeqM.fold_left
   let test_iteri =
     test_of_ty "iteri"
       DSL.([ [data; int; data] @-> data; data; seq data ] --> monad data)
       (fun f init s ->
         let r = ref init in
         Seq.iteri (fun i x -> r := f !r i x) s;
         !r)
       (fun f init s ->
         let r = ref init in
         Mon.bind
           (SeqM.iteri
             (fun i x ->
                CallerMon.bind
                  (f !r i x)
                  (fun rr -> r := rr; CallerMon.return ()))
             s)
           (fun () -> Mon.return !r))
   let test_fold_lefti =
     test_of_ty "fold_lefti"
       DSL.([ [data; int; data] @-> data; data; seq data ] --> monad data)
       Seq.fold_lefti
       SeqM.fold_lefti
   let test_for_all =
     test_of_ty "for_all"
       DSL.([[data] @-> bool; seq data] --> monad bool)
       Seq.for_all
       SeqM.for_all
   let test_exists =
     test_of_ty "exists"
       DSL.([[data] @-> bool; seq data] --> monad bool)
       Seq.exists
       SeqM.exists
   let test_find =
     test_of_ty "find"
       DSL.([[data] @-> bool; seq data] --> monad (option data))
       Seq.find
       SeqM.find
   let test_find_map =
     test_of_ty "find_map"
       DSL.([[data] @-> option data; seq data] --> monad (option data))
       Seq.find_map
       SeqM.find_map
   let test_iter2 =
     test_of_ty "iter2"
       DSL.([ [data; data; data] @-> data; data; seq data; seq data; ] --> monad data)
       (fun f init xs ys ->
         let r = ref init in
         Seq.iter2 (fun x y -> r := f !r x y) xs ys;
         !r)
       (fun f init xs ys ->
         let r = ref init in
         Mon.bind
           (SeqM.iter2
             (fun x y ->
                CallerMon.bind
                    (f !r x y)
                    (fun rr -> r := rr; CallerMon.return ()))
             xs
             ys)
           (fun () -> Mon.return !r))
   let test_fold_left2 =
     test_of_ty "fold_left2"
       DSL.([ [data; data; data] @-> data; data; seq data; seq data; ] --> monad data)
       Seq.fold_left2
       SeqM.fold_left2
   let test_for_all2 =
     test_of_ty "for_all2"
       DSL.([[data; data] @-> bool; seq data; seq data] --> monad bool)
       Seq.for_all2
       SeqM.for_all2
   let test_exists2 =
     test_of_ty "exists2"
       DSL.([[data; data] @-> bool; seq data; seq data] --> monad bool)
       Seq.exists2
       SeqM.exists2

   let tests : _ list = [
      test_equal ;
      test_compare ;
      test_iter;
      test_fold_left;
      test_iteri;
      test_fold_lefti ;
      test_for_all ;
      test_exists ;
      test_find ;
      test_find_map ;
      test_iter2 ;
      test_fold_left2;
      test_for_all2 ;
      test_exists2 ;
   ]

end

module MakeTransformers1Tests
   (Support : SUPPORT1)
   (SeqM
: Seqes.Sigs.SEQMON1TRANSFORMERS
    with type 'a mon := 'a Support.Mon.t
    with type 'a callermon := 'a Support.CallerMon.t
    with type 'a t := 'a Support.seqm)
: sig
   val tests : unit Alcotest.test_case list
end
= struct

   open Support

   let test_init =
     test_of_ty "init"
       DSL.([nat; [int] @-> data] --> seq data)
       Seq.init
       SeqM.init
   let test_unfold =
     test_of_ty "unfold"
       DSL.([[int] @-> option (data * int); int] --> seq data)
       Seq.unfold
       SeqM.unfold
   let test_forever =
     test_of_ty "forever"
       DSL.([[unit] @-> data] --> seq data)
       Seq.forever
       SeqM.forever
   let test_iterate =
     test_of_ty "iterate"
       DSL.([[data] @-> data; data] --> seq data)
       Seq.iterate
       SeqM.iterate
   let test_map =
     test_of_ty "map"
       DSL.([[data] @-> data; seq data] --> seq data)
       Seq.map
       SeqM.map
   let test_mapi =
     test_of_ty "mapi"
       DSL.([[int; data] @-> data; seq data] --> seq data)
       Seq.mapi
       SeqM.mapi
   let test_filter =
     test_of_ty "filter"
       DSL.([[data] @-> bool; seq data] --> seq data)
       Seq.filter
       SeqM.filter
   let test_filter_map =
     test_of_ty "filter_map"
       DSL.([[data] @-> (option data); seq data] --> seq data)
       Seq.filter_map
       SeqM.filter_map
   let test_flat_map =
     test_of_ty "flat_map"
       DSL.([[data] @-> seq data; seq data] --> seq data)
       Seq.flat_map
       SeqM.flat_map
   let test_scan =
     test_of_ty "scan"
       DSL.([[data; data] @-> data; data; seq data] --> seq data)
       Seq.scan
       SeqM.scan
   let test_sorted_merge =
     test_of_ty "sorted_merge"
       DSL.([[data; data] @-> int; seq data; seq data] --> seq data)
       Seq.sorted_merge
       SeqM.sorted_merge
   let test_take_while =
     test_of_ty "take_while"
       DSL.([[data] @-> bool; seq data] --> seq data)
       Seq.take_while
       SeqM.take_while
   let test_drop_while =
     test_of_ty "drop_while"
       DSL.([[data] @-> bool; seq data] --> seq data)
       Seq.drop_while
       SeqM.drop_while
   let test_group =
     test_of_ty "group"
       DSL.([[data; data] @-> bool; seq data] --> seq (seq data))
       Seq.group
       SeqM.group
   let test_map2 =
     test_of_ty "map2"
       DSL.([[data; data] @-> data; seq data; seq data] --> seq data)
       Seq.map2
       SeqM.map2
   let test_map_product =
     test_of_ty "map_product"
       DSL.([[data; data] @-> data; seq data; seq data] --> seq data)
       Seq.map_product
       SeqM.map_product
   let test_partition_map =
     test_of_ty "partition_map"
       DSL.([[data] @-> either data data; seq data] --> (seq data * seq data))
       Seq.partition_map
       SeqM.partition_map
   let test_partition =
     test_of_ty "partition"
       DSL.([[data] @-> bool; seq data] --> (seq data * seq data))
       Seq.partition
       SeqM.partition
   let test_of_dispenser =
     test_of_ty "of_dispenser"
       DSL.([[unit] @-> (option data)] --> seq data)
       Seq.of_dispenser
       SeqM.of_dispenser

   module TraversorsTests = MakeTraversors1Tests(Support)(SeqM)

   let tests : unit Alcotest.test_case list =
      TraversorsTests.tests @ [
      test_init ;
      test_unfold ;
      test_forever ;
      test_iterate ;
      test_map ;
      test_mapi ;
      test_filter ;
      test_filter_map ;
      test_scan ;
      test_sorted_merge ;
      test_flat_map ;
      (*   test_concat_map ; <-- concat_map is an alias for flat_map *)
      test_take_while ;
      test_drop_while ;
      test_group ;
      test_map2 ;
      test_map_product ;
      test_partition_map ;
      test_partition ;
      test_of_dispenser ;
   ]

end

module MakeAll1Tests
   (Support : SUPPORT1 with module CallerMon = TestMonads.TransparentId1)
   (SeqM : Seqes.Sigs.SEQMON1ALL
       with type 'a mon := 'a Support.Mon.t
       with type 'a t := 'a Support.seqm)
: sig
   val tests : unit Alcotest.test_case list
end
= struct

   open Support

   let test_is_empty =
     test_of_ty "is_empty"
       DSL.([seq data] --> monad bool)
       Seq.is_empty
       SeqM.is_empty
   let test_uncons =
     test_of_ty "uncons"
       DSL.([seq data] --> monad (option (data * seq data)))
       Seq.uncons
       SeqM.uncons
   let test_length =
     test_of_ty "length"
       DSL.([seq data] --> monad int)
       Seq.length
       SeqM.length
   let test_empty =
     test_of_ty "empty"
       DSL.([unit] --> seq data)
       (fun () -> Seq.empty)
       (fun () -> SeqM.empty)
   let test_return =
     test_of_ty "return"
       DSL.([data] --> seq data)
       Seq.return
       SeqM.return
   let test_cons =
     test_of_ty "cons"
       DSL.([data; seq data] --> seq data)
       Seq.cons
       SeqM.cons
   let test_repeat =
     test_of_ty "repeat"
       DSL.([data] --> seq data)
       Seq.repeat
       SeqM.repeat
   let test_cycle =
     test_of_ty "cycle"
       DSL.([seq data] --> seq data)
       Seq.cycle
       SeqM.cycle
   let test_take =
     test_of_ty "take"
       DSL.([nat; seq data] --> seq data)
       Seq.take
       SeqM.take
   let test_drop =
     test_of_ty "drop"
       DSL.([nat; seq data] --> seq data)
       Seq.drop
       SeqM.drop
   let test_transpose =
     test_of_ty "transpose"
       DSL.([seq (seq data)] --> seq (seq data))
       Seq.transpose
       SeqM.transpose
   let test_append =
     test_of_ty "append"
       DSL.([seq data; seq data] --> seq data)
       Seq.append
       SeqM.append
   let test_concat =
     test_of_ty "concat"
       DSL.([seq (seq data)] --> seq data)
       Seq.concat
       SeqM.concat
   let test_zip =
     test_of_ty "zip"
       DSL.([seq data; seq data] --> seq (data * data))
       Seq.zip
       SeqM.zip
   let test_interleave =
     test_of_ty "interleave"
       DSL.([seq data; seq data] --> seq data)
       Seq.interleave
       SeqM.interleave
   let test_product =
     test_of_ty "product"
       DSL.([seq data; seq data] --> seq (data * data))
       Seq.product
       SeqM.product
   let test_unzip =
     test_of_ty "unzip (split)"
       DSL.([seq (data * data)] --> (seq data * seq data))
       Seq.unzip
       SeqM.unzip
   let test_to_dispenser =
     test_of_ty "to_dispenser"
       DSL.([seq data] --> ([unit] ->> option data))
       Seq.to_dispenser
       SeqM.to_dispenser
   let test_ints =
     test_of_ty "ints"
       DSL.([int] --> seq int)
       Seq.ints
       SeqM.ints

   module TransformersTests = MakeTransformers1Tests(Support)(SeqM)

   let tests : unit Alcotest.test_case list =
      TransformersTests.tests @ [
      test_is_empty ;
      test_uncons ;
      test_length ;
      test_empty ;
      test_return ;
      test_cons ;
      test_repeat ;
      test_cycle ;
      test_take ;
      test_drop ;
    (* TODO:
      (* [memoize] and [once] are about multiple traversal and side-effects, they
         need a different kind of testing as the other functions. *)
      test_memoize ;
      test_once ;
    *)
      test_transpose ;
      test_append ;
      test_concat ;
      test_zip ;
      test_interleave ;
      test_product ;
      test_unzip ;
    (*   test_split ; <-- split is an alias for unzip *)
      test_to_dispenser ;
      test_ints ;
   ]

end


(* Support module for tests relating to 2-parameter monads *)

module type SUPPORT2
= sig

   (* The Monad that the sequences are parametrised over *)
   module Mon : Seqes.Sigs.MONAD2

   (* The Monad that the sequences traversors are called with *)
   module CallerMon : Seqes.Sigs.MONAD2

   (* The type of the monadic sequence *)
   type ('a, 'e) seqm

   (* The type of values that describe types. Specifically they describe the
      interface of different parts of Seqes. See [DSL] below for constructors *)
   type (_, _, _) ty
   and (_, _, _, _, _, _) params =
   | [] : ('vr, 'vr, 'mr, 'mr, 'wr, 'wr) params
   | ( :: ) : ('vp, 'mp, 'wp) ty * ('vk, 'vr, 'mk, 'mr, 'wk, 'wr) params ->
       ('vp -> 'vk, 'vr, 'mp -> 'mk, 'mr, 'wp -> 'wk, 'wr) params

   (* The eDSL to describe functions *)
   module DSL : sig
      val unit : (unit, unit, unit) ty
      val data : (char, char, char) ty (* used to describe data: the elements of the sequences *)
      val int : (int, int, int) ty
      val nat : (int, int, int) ty
      val bool : (bool, bool, bool) ty
      val ( * ) : ('a, 'b, 'c) ty -> ('u, 'v, 'w) ty -> ('a * 'u, 'b * 'v, 'c * 'w) ty
      val option : ('a, 'b, 'c) ty -> ('a option, 'b option, 'c option) ty
      val either : ('a, 'b, 'c) ty -> ('u, 'v, 'w) ty -> (('a, 'u) Either.t, ('b, 'v) Either.t, ('c, 'w) Either.t) ty
      val seq : ('a, 'b, 'c) ty -> ('a Seq.t, ('b, 'bb) seqm, 'b) ty
      val monad : ('a, 'b, 'c) ty -> ('a, ('b, 'bb) Mon.t, 'b) ty
      val callermonad : ('a, 'b, 'c) ty -> ('a, ('b, 'bb) CallerMon.t, 'b) ty
      val ( @-> ) : ('a, 'b, 'c, ('d, 'dd) CallerMon.t, 'e, 'd) params -> ('b, 'd, 'f) ty -> ('a, 'c, 'd) ty
      val ( ->> ) : ('a, 'b, 'c, ('d, 'dd) Mon.t, 'e, 'd) params -> ('b, 'd, 'f) ty -> ('a, 'c, 'd) ty
      val ( --> ) : ('a, 'b, 'c, 'd, 'e, 'f) params -> ('b, 'd, 'f) ty -> ('a, 'b, 'c, 'd, 'e, 'f) params * ('b, 'd, 'f) ty
   end

   (* A function to convert a type description into a test *)
   val test_of_ty : string -> ('a, 'b, 'c, 'd, 'e, 'f) params * ('b, 'd, 'f) ty -> 'a -> 'c -> unit Alcotest.test_case

end

(* Generates support modules from monads and such *)
module MakeSupport2
   (SeqMon : TestMonads.M2)
   (Mon : TestMonads.M2)
   (CallerMon : TestMonads.M2)
   (SeqM : sig (* only use this part of the monadic seq *)
      type ('a, 'e) t
      val of_seq : 'a Seq.t -> ('a, 'e) t
      val map : ('a -> 'b) -> ('a, 'e) t -> ('b, 'e) t
      val uncons : ('a, 'e) t -> (('a * ('a, 'e) t) option, 'e) SeqMon.t
   end)
: SUPPORT2
   with module Mon = Mon
   with module CallerMon = CallerMon
   with type ('a, 'e) seqm = ('a, 'e) SeqM.t
= struct
   module Mon = Mon
   module CallerMon = CallerMon
   type ('a, 'e) seqm = ('a, 'e) SeqM.t

   (* QCheck2 generator for sequences *)
   module GenSeq
   : sig
     val small : ('a * 'b) QCheck2.Gen.t -> ('a Seq.t * ('b, 'e) SeqM.t) QCheck2.Gen.t
   end
   = struct

      let small g =
         QCheck2.Gen.map
            (fun vml ->
               let vms = List.to_seq vml in
               let vs, ms = Seq.unzip vms in
               (vs, SeqM.of_seq ms))
            (QCheck2.Gen.oneof [
               QCheck2.Gen.pure [];
               QCheck2.Gen.small_list g;
            ])

   end


   (* The [ty] type is for describing items of the Seq interface. Each variant of
      [ty] corresponds to a type (e.g., [Unit] is for [unit]), a type variable
      (e.g., [Char] is used for the element of the sequences), or a type
      constructor (e.g., [Option] is for [option]).

      The two first parameters are for plain (a.k.a., [v] for value) and monadic
      (a.k.a. [m]) variants. We need both descriptions because we compare the
      monadic functions of the [SeqM] functor parameter to the plain functions
      of [Stdlib.Seq].

      The third parameter is for injectivity: we give the type checker the
      parameter of the monadic type to hold, so that it can't be erased by some
      later substitution.

      The lambdas are described as [Lambda (params, return)] where [params] encodes
      all the types of all the parameters of the lambda and [return] encodes the
      type of the return value of the lambda. The first type parameter of [params]
      is the full lambda type (with the [->]), the second is the return type, the
      third is the [->] but for the monad side of things, the last is the return
      but for the monad side of things. E.g., [Lambda ([Char; Char], Bool)] is for
      a lambda of the type [(char -> char -> bool)]. *)
   type (_, _, _) ty =
     | Unit : (unit, unit, unit) ty
     | Char : (char, char, char) ty (* use for the elements of sequences, see [data] below *)
     | Int : (int, int, int) ty
     | Nat : (int, int, int) ty (* positive integers for indexes and lengths *)
     | Bool : (bool, bool, bool) ty
     | Tup2 : ('va, 'ma, 'wa) ty * ('vb, 'mb, 'wb) ty -> (('va * 'vb), ('ma * 'mb), ('wa * 'wb)) ty
     | Option : ('v, 'm, 'w) ty -> ('v option, 'm option, 'w option) ty
     | Either : ('va, 'ma, 'wa) ty * ('vb, 'mb, 'wb) ty -> (('va, 'vb) Either.t, ('ma, 'mb) Either.t, ('wa, 'wb) Either.t) ty
     | Lambda : ('vk, 'vr, 'mk, 'mr, 'wk, 'wr) params * ('vr, 'mr, 'wr) ty -> ('vk, 'mk, 'wr) ty
     | Seq : ('va, 'ma, _) ty -> ('va Seq.t, ('ma, 'mmaa) SeqM.t, 'ma) ty
     | Monad : ('va, 'ma, _) ty -> ('va, ('ma, 'mmaa) Mon.t, 'ma) ty
     | CallerMonad : ('va, 'ma, _) ty -> ('va, ('ma, 'mmaa) CallerMon.t, 'ma) ty
   and (_, _, _, _, _, _) params =
     | [] : ('vr, 'vr, 'mr, 'mr, 'wr, 'wr) params
     | ( :: ) : ('vp, 'mp, 'wp) ty * ('vk, 'vr, 'mk, 'mr, 'wk, 'wr) params ->
         ('vp -> 'vk, 'vr, 'mp -> 'mk, 'mr, 'wp -> 'wk, 'wr) params

   (* Helper syntax for describing items of the Seq interface *)
   module DSL = struct
      let unit = Unit
      let data = Char (* used to describe data: the elements of the sequences *)
      let int = Int
      let nat = Nat
      let bool = Bool
      let ( * ) a b = Tup2 (a, b)
      let option a = Option a
      let either a b = Either (a, b)
      let seq a = Seq a
      let monad a = Monad a
      let callermonad a = CallerMonad a
      let ( @-> ) p r = Lambda (p, CallerMonad r) (* used for inner lambda, hence caller *)
      let ( ->> ) p r = Lambda (p, Monad r) (* used for to_dispenser only *)
      let ( --> ) p r = (* used for top-level lambdas *)
         let _ = Lambda (p, r) in (* this imposes some restrictions on the types *)
         (p, r) (* we don't want it wrapped in a [Lambda] constructor for practical reasons *)
   end

   (* makes the monad version of a plain value (based on the the ty descriptor) *)
   let rec monadify_of_ty : type a b c. (a, b, c) ty -> a -> b = function
     | Unit -> fun () -> ()
     | Char -> fun a -> a
     | Int -> fun a -> a
     | Nat -> fun a -> a
     | Bool -> fun a -> a
     | Tup2 (tyx, tyy) -> fun (x, y) ->
         let monadifier_x = monadify_of_ty tyx in
         let monadifier_y = monadify_of_ty tyy in
         (monadifier_x x, monadifier_y y)
     | Option ty -> (function
         | Some a ->
             let monadifier = monadify_of_ty ty in
             Some (monadifier a)
         | None -> None
     )
     | Either (tyl, tyr) -> (function
         | Either.Left l ->
             let monadifier = monadify_of_ty tyl in
             Either.Left (monadifier l)
         | Either.Right r ->
             let monadifier = monadify_of_ty tyr in
             Either.Right (monadifier r)
     )
     | Lambda _ -> invalid_arg "monadifier_of_ty.Lambda: unimplemented"
     | Seq ty -> fun s ->
         let monadifier = monadify_of_ty ty in
         SeqM.of_seq (Seq.map monadifier s)
     | Monad ty -> (fun v ->
         let monadifier = monadify_of_ty ty in
         Mon.return (monadifier v))
     | CallerMonad ty -> (fun v ->
         let monadifier = monadify_of_ty ty in
         CallerMon.return (monadifier v))

   (* makes the plain version of a monad value (based on the the ty descriptor) *)
   let rec unmonadify_of_ty : type a b c. (a, b, c) ty -> b -> a = function
     | Unit -> fun () -> ()
     | Char -> fun b -> b
     | Int -> fun b -> b
     | Nat -> fun b -> b
     | Bool -> fun b -> b
     | Tup2 (tyx, tyy) -> fun (x, y) ->
         let unmonadifier_x = unmonadify_of_ty tyx in
         let unmonadifier_y = unmonadify_of_ty tyy in
         (unmonadifier_x x, unmonadifier_y y)
     | Option ty -> (function
         | Some a ->
             let unmonadifier = unmonadify_of_ty ty in
             Some (unmonadifier a)
         | None -> None
     )
     | Either (tyl, tyr) -> (function
         | Either.Left l ->
             let unmonadifier = unmonadify_of_ty tyl in
             Either.Left (unmonadifier l)
         | Either.Right r ->
             let unmonadifier = unmonadify_of_ty tyr in
             Either.Right (unmonadifier r)
     )
     | Lambda _ -> invalid_arg "unmonadifier_of_ty.Lambda: unimplemented"
     | Seq ty -> fun s ->
         let unmonadifier = unmonadify_of_ty ty in
         seq_of_seqm (SeqM.map unmonadifier s)
     | Monad ty -> (fun v ->
         let unmonadifier = unmonadify_of_ty ty in
         unmonadifier (Mon.break v))
     | CallerMonad ty -> (fun v ->
         let unmonadifier = unmonadify_of_ty ty in
         unmonadifier (CallerMon.break v))
   and seq_of_seqm : type a e. (a, e) SeqM.t -> a Seq.t = fun seqm ->
      fun () ->
         match SeqMon.break (SeqM.uncons seqm) with
               | None -> Seq.Nil
               | Some (v, seqm) -> Seq.Cons (v, (seq_of_seqm seqm))


   let rec eq_seq_seqm n eq seq seqm =
     n < 0 || begin
     match seq (), SeqMon.break (SeqM.uncons seqm) with
     | Stdlib.Seq.Nil, None -> true
     | Stdlib.Seq.Cons (v, seq), Some (m, seqm) ->
         eq v m && eq_seq_seqm (n-1) eq seq seqm
     | _ -> false
     end

   let eq_dispenser_dispenserm n eq dis dism =
     let rec go n =
       n < 0 || begin
       match dis (), Mon.break (dism ()) with
       | None, None -> true
       | Some v, Some m -> eq v m && go (n-1)
       | _ -> false
       end
     in
     go n

   (* compares a plain and monadic values described by a ty; note that sequences
      are potentially infinite so we limit sequence comparison to the first 100
      elements *)
   let rec eq_of_ty : type a b c. (a, b, c) ty -> (a -> b -> bool) = function
     | Unit -> fun () () -> true
     | Char -> fun a b -> a = b
     | Int -> fun a b -> a = b
     | Nat -> fun a b -> a = b
     | Bool -> fun a b -> a = b
     | Tup2 (tyx, tyy) -> fun (xa, ya) (xb, yb) -> eq_of_ty tyx xa xb && eq_of_ty tyy ya yb
     | Option ty -> (fun a b -> match a, b with
         | Some a, Some b -> eq_of_ty ty a b
         | None, None -> true
         | Some _, None -> false
         | None, Some _ -> false
     )
     | Either (tyl, tyr) -> (fun a b ->
         match a, b with
         | Either.Left a, Either.Left b ->
             let eql = eq_of_ty tyl in
             eql a b
         | Either.Right a, Either.Right b ->
             let eqr = eq_of_ty tyr in
             eqr a b
         | _ -> false
     )
     | Lambda ([Unit], Monad (Option Char)) -> (* special case needed for comparing [to_dispenser] *)
         fun (a : unit -> char option) (b : unit -> (char option, _) Mon.t) ->
           eq_dispenser_dispenserm 100 Char.equal a b
     | Lambda _ -> invalid_arg "eq_of_ty.Lambda: impossible"
     | Seq ty -> (fun sa sb -> eq_seq_seqm 100 (eq_of_ty ty) sa sb)
     | Monad ty -> (fun v m -> (eq_of_ty ty) v (Mon.break m))
     | CallerMonad ty -> (fun v m -> (eq_of_ty ty) v (CallerMon.break m))

   (* in order to generate lambdas, we need observables *)
   let rec observable_of_ty : type a b c. (a, b, c) ty -> a QCheck2.Observable.t = function
     | Unit -> QCheck2.Observable.unit
     | Char -> QCheck2.Observable.char
     | Int -> QCheck2.Observable.int
     | Nat -> QCheck2.Observable.int
     | Bool -> QCheck2.Observable.bool
     | Tup2 (tyx, tyy) ->
         let ox = observable_of_ty tyx in
         let oy = observable_of_ty tyy in
         QCheck2.Observable.pair ox oy
     | Option ty ->
         let o = observable_of_ty ty in
         QCheck2.Observable.option o
     | Either _ ->
         (* never appears inside another lambda so we don't need it *)
         invalid_arg "observable_of_ty.Either: unimplemented"
     | Lambda _ ->
         (* never appears inside another lambda so we don't need it *)
         invalid_arg "observable_of_ty.Lambda: unimplemented"
     | Seq ty ->
         let o = observable_of_ty ty in
         QCheck2.Observable.map List.of_seq (QCheck2.Observable.list o)
     | Monad ty -> observable_of_ty ty
     | CallerMonad ty -> observable_of_ty ty

   (* for showing test results and such, we need to be able to pretty-print values *)
   let rec to_string_of_ty
     : type v m w. (v, m, w) ty -> (v QCheck2.Print.t * m QCheck2.Print.t)
     = fun ty -> match ty with
     | Unit -> QCheck2.Print.unit, QCheck2.Print.unit
     | Char -> QCheck2.Print.char, QCheck2.Print.char
     | Int -> QCheck2.Print.int, QCheck2.Print.int
     | Nat -> QCheck2.Print.int, QCheck2.Print.int
     | Bool -> QCheck2.Print.bool, QCheck2.Print.bool
     | Tup2 (tyx, tyy) ->
         let (pvx, pmx) = to_string_of_ty tyx in
         let (pvy, pmy) = to_string_of_ty tyy in
         ( (fun (x, y) -> Format.asprintf "(%s,%s)" (pvx x) (pvy y)) ,
           (fun (x, y) -> Format.asprintf "(%s,%s)" (pmx x) (pmy y)) )
     | Option ty ->
         let (pvx, pmx) = to_string_of_ty ty in
         ( (function
             | None -> "None"
             | Some x -> Format.asprintf "Some (%s)" (pvx x)) ,
           (function
             | None -> "None"
             | Some x -> Format.asprintf "Some (%s)" (pmx x)) )
     | Either (tya, tyb) ->
         let (pva, pma) = to_string_of_ty tya in
         let (pvb, pmb) = to_string_of_ty tyb in
         ( (function
             | Either.Left a -> Format.asprintf "Left (%s)" (pva a)
             | Either.Right b -> Format.asprintf "Right (%s)" (pvb b)) ,
           (function
             | Either.Left a -> Format.asprintf "Left (%s)" (pma a)
             | Either.Right b -> Format.asprintf "Right (%s)" (pmb b)) )
     | Lambda (_, _) ->
         ( (fun _ -> "\\") ,
           (fun _ -> "\\") )
     | Seq ty ->
         let (pvx, pmx) = to_string_of_ty ty in
         ( (fun s ->
             Format.asprintf "Seq (%s)"
               (String.concat " " (List.map pvx (List.of_seq s)))) ,
           (fun s ->
             Format.asprintf "Seq (%s)"
               (String.concat " " (List.map pmx (List.of_seq (seq_of_seqm s))))))
     | Monad ty ->
         let (pvx, pmx) = to_string_of_ty ty in
         ( pvx ,
           (fun m -> Format.asprintf "Monad (%s)" (pmx (Mon.break m))) )
     | CallerMonad ty ->
         let (pvx, pmx) = to_string_of_ty ty in
         ( pvx ,
           (fun m -> Format.asprintf "CallerMonad (%s)" (pmx (CallerMon.break m))) )

   (* QCheck2 generators, derived from [ty] descriptions. *)
   let rec gen_of_ty
     : type v m w. (v, m, w) ty -> (v * m) QCheck2.Gen.t
     = fun ty -> match ty with
     | Unit -> QCheck2.Gen.map (fun () -> ((), ())) QCheck2.Gen.unit
     | Char -> QCheck2.Gen.map (fun x -> (x, x)) QCheck2.Gen.printable
     | Int -> QCheck2.Gen.map (fun x -> (x, x)) QCheck2.Gen.int
     | Nat -> QCheck2.Gen.map (fun x -> (x, x)) QCheck2.Gen.nat
     | Bool -> QCheck2.Gen.map (fun x -> (x, x)) QCheck2.Gen.bool
     | Tup2 (tyx, tyy) ->
         let gx = gen_of_ty tyx in
         let gy = gen_of_ty tyy in
         QCheck2.Gen.map2 (fun (vx, mx) (vy, my) -> ((vx, vy), (mx, my))) gx gy
     | Option ty ->
         QCheck2.Gen.oneof [
           QCheck2.Gen.return (None, None);
           QCheck2.Gen.map (fun (v, m) -> (Some v, Some m)) (gen_of_ty ty);
         ]
     | Either (tya, tyb) ->
         let ga = gen_of_ty tya in
         let gb = gen_of_ty tyb in
         QCheck2.Gen.oneof [
           QCheck2.Gen.map (fun (va, ma) -> Either.Left va, Either.Left ma) ga ;
           QCheck2.Gen.map (fun (vb, mb) -> Either.Right vb, Either.Right mb) gb ;
         ]
     | Lambda (params, tyr) ->
         gen_of_params params tyr
     | Seq ty ->
         let g = gen_of_ty ty in
         GenSeq.small g
     | Monad ty ->
         let g = gen_of_ty ty in
         QCheck2.Gen.map (fun (v, m) -> (v, Mon.return m)) g
     | CallerMonad ty ->
         let g = gen_of_ty ty in
         QCheck2.Gen.map (fun (v, m) -> (v, CallerMon.return m)) g
   and gen_of_params
     : type vk vr mk mr wk wr. (vk, vr, mk, mr, wk, wr) params -> (vr, mr, wr) ty -> (vk * mk) QCheck2.Gen.t
     = fun params tyr ->
       match params with
       | [] ->
           (* zero-parameters functions don't exist *)
           assert false
       | [tya] ->
           let gvr = QCheck2.Gen.map fst @@ gen_of_ty tyr in
           let aov = observable_of_ty tya in
           let fg = QCheck2.fun1 aov gvr in
           QCheck2.Gen.map (fun (QCheck2.Fun (_, f)) ->
             (* The two functions need to be identical (up to the monad), so we
                just generate a single vanilla one ([f]) and make the monadic
                variant by wrapping it with the monadification and
                unmonadification functions. *)
             let unmonadifier_a = unmonadify_of_ty tya in
             let monadifier_r = monadify_of_ty tyr in
             let fm a = monadifier_r (f (unmonadifier_a a)) in
             (f, fm)
           ) fg
       | [tya; tyb] ->
           let gvr = QCheck2.Gen.map fst @@ gen_of_ty tyr in
           let aov = observable_of_ty tya in
           let bov = observable_of_ty tyb in
           let fg = QCheck2.fun2 aov bov gvr in
           QCheck2.Gen.map (fun (QCheck2.Fun (_, f)) ->
             let unmonadifier_a = unmonadify_of_ty tya in
             let unmonadifier_b = unmonadify_of_ty tyb in
             let monadifier_r = monadify_of_ty tyr in
             let fm a b = monadifier_r (f (unmonadifier_a a) (unmonadifier_b b)) in
             (f, fm)
           ) fg
       | [tya; tyb; tyc] ->
           let gvr = QCheck2.Gen.map fst @@ gen_of_ty tyr in
           let aov = observable_of_ty tya in
           let bov = observable_of_ty tyb in
           let cov = observable_of_ty tyc in
           let fg = QCheck2.fun3 aov bov cov gvr in
           QCheck2.Gen.map (fun (QCheck2.Fun (_, f)) ->
             let unmonadifier_a = unmonadify_of_ty tya in
             let unmonadifier_b = unmonadify_of_ty tyb in
             let unmonadifier_c = unmonadify_of_ty tyc in
             let monadifier_r = monadify_of_ty tyr in
             let fm a b c = monadifier_r (f (unmonadifier_a a) (unmonadifier_b b) (unmonadifier_c c)) in
             (f, fm)
           ) fg
       | [tya; tyb; tyc; tyd] ->
           let gvr = QCheck2.Gen.map fst @@ gen_of_ty tyr in
           let aov = observable_of_ty tya in
           let bov = observable_of_ty tyb in
           let cov = observable_of_ty tyc in
           let dov = observable_of_ty tyd in
           let fg = QCheck2.fun4 aov bov cov dov gvr in
           QCheck2.Gen.map (fun (QCheck2.Fun (_, f)) ->
             let unmonadifier_a = unmonadify_of_ty tya in
             let unmonadifier_b = unmonadify_of_ty tyb in
             let unmonadifier_c = unmonadify_of_ty tyc in
             let unmonadifier_d = unmonadify_of_ty tyd in
             let monadifier_r = monadify_of_ty tyr in
             let fm a b c d = monadifier_r (f (unmonadifier_a a) (unmonadifier_b b) (unmonadifier_c c) (unmonadifier_d d)) in
             (f, fm)
           ) fg
       | _ ->
           (* the maximum arity of lambdas in the Seq interface is 4 so we don't
              need more *)
           invalid_arg "gen_of_params._: unimplemented arity"

   let test_of_ty
   : type vk vr mk mr wk wr. string -> (vk, vr, mk, mr, wk, wr) params -> (vr, mr, wr) ty ->
     vk -> mk -> QCheck2.Test.t
   = fun name params tyr vf mf ->
     match params with
     | [] ->
         (* there are no lambdas with zero parameters *)
         assert false
     | [tya] ->
         QCheck2.Test.make
           ~name
           ~print:(fun (va, ma) ->
             let (pv, pm) = to_string_of_ty tya in
             Format.asprintf "{V: %s; M: %s}"
               (pv va)
               (pm ma)
           )
           (gen_of_ty tya)
           (fun (va, ma) ->
             (eq_of_ty tyr)
               (vf va)
               (mf ma))
     | [tya; tyb] ->
         let ag = gen_of_ty tya in
         let bg = gen_of_ty tyb in
         QCheck2.Test.make
           ~name
           ~print:(fun ((va, ma), (vb, mb)) ->
             let (pav, pam) = to_string_of_ty tya in
             let (pbv, pbm) = to_string_of_ty tyb in
             Format.asprintf "{V: (%s, %s); M: (%s, %s)}"
               (pav va) (pbv vb)
               (pam ma) (pbm mb)
           )
           (QCheck2.Gen.tup2 ag bg)
           (fun ((va, ma), (vb, mb)) ->
             (eq_of_ty tyr)
               (vf va vb)
               (mf ma mb))
     | [tya; tyb; tyc] ->
         let ag = gen_of_ty tya in
         let bg = gen_of_ty tyb in
         let cg = gen_of_ty tyc in
         QCheck2.Test.make
           ~name
           ~print:(fun ((va, ma), (vb, mb), (vc, mc)) ->
             let (pav, pam) = to_string_of_ty tya in
             let (pbv, pbm) = to_string_of_ty tyb in
             let (pcv, pcm) = to_string_of_ty tyc in
             Format.asprintf "{V: (%s, %s, %s); M: (%s, %s, %s)}"
               (pav va) (pbv vb) (pcv vc)
               (pam ma) (pbm mb) (pcm mc)
           )
           (QCheck2.Gen.tup3 ag bg cg)
           (fun ((va, ma), (vb, mb), (vc, mc)) ->
             (eq_of_ty tyr)
               (vf va vb vc)
               (mf ma mb mc))
     | [tya; tyb; tyc; tyd] ->
         let ag = gen_of_ty tya in
         let bg = gen_of_ty tyb in
         let cg = gen_of_ty tyc in
         let dg = gen_of_ty tyd in
         QCheck2.Test.make
           ~name
           (QCheck2.Gen.tup4 ag bg cg dg)
           (fun ((va, ma), (vb, mb), (vc, mc), (vd, md)) ->
             (eq_of_ty tyr)
               (vf va vb vc vd)
               (mf ma mb mc md))
     | _ ->
         (* the maximum arity of Seq interface item is 4 so we don't need more *)
         invalid_arg "test_of_ty._: unimplemented arity"

   let test_of_ty name (params, tyr) vf mf =
     QCheck_alcotest.to_alcotest @@ test_of_ty name params tyr vf mf

end

module MakeTraversors2Tests
   (Support : SUPPORT2)
   (SeqM
: Seqes.Sigs.SEQMON2TRAVERSORS
    with type ('a, 'e) mon := ('a, 'e) Support.Mon.t
    with type ('a, 'e) callermon := ('a, 'e) Support.CallerMon.t
    with type ('a, 'e) t := ('a, 'e) Support.seqm)
: sig
   val tests : unit Alcotest.test_case list
end
= struct

   open Support

   let test_equal =
     test_of_ty "equal"
       DSL.([[data; data] @-> bool; seq data; seq data] --> monad bool)
       Seq.equal
       SeqM.equal
   let test_compare =
     test_of_ty "compare"
       DSL.([[data; data] @-> int; seq data; seq data] --> monad int)
       Seq.compare
       SeqM.compare
   let test_iter =
     test_of_ty "iter"
       DSL.([ [data; data] @-> data; data; seq data ] --> monad data)
       (fun f init s ->
         let r = ref init in
         Seq.iter (fun x -> r := f !r x) s;
         !r)
       (fun f init s ->
         (* Because we want to observe side-effects, we have to do some extra
            legwork around the monad *)
         let r = ref init in
         Mon.bind
           (SeqM.iter
              (fun x ->
                 CallerMon.bind
                   (f !r x)
                   (fun rr -> r := rr; CallerMon.return ()))
              s)
           (fun () -> Mon.return !r))
   let test_fold_left =
     test_of_ty "fold_left"
       DSL.([ [data; data] @-> data; data; seq data ] --> monad data)
       Seq.fold_left
       SeqM.fold_left
   let test_iteri =
     test_of_ty "iteri"
       DSL.([ [data; int; data] @-> data; data; seq data ] --> monad data)
       (fun f init s ->
         let r = ref init in
         Seq.iteri (fun i x -> r := f !r i x) s;
         !r)
       (fun f init s ->
         let r = ref init in
         Mon.bind
           (SeqM.iteri
             (fun i x ->
                CallerMon.bind
                  (f !r i x)
                  (fun rr -> r := rr; CallerMon.return ()))
             s)
           (fun () -> Mon.return !r))
   let test_fold_lefti =
     test_of_ty "fold_lefti"
       DSL.([ [data; int; data] @-> data; data; seq data ] --> monad data)
       Seq.fold_lefti
       SeqM.fold_lefti
   let test_for_all =
     test_of_ty "for_all"
       DSL.([[data] @-> bool; seq data] --> monad bool)
       Seq.for_all
       SeqM.for_all
   let test_exists =
     test_of_ty "exists"
       DSL.([[data] @-> bool; seq data] --> monad bool)
       Seq.exists
       SeqM.exists
   let test_find =
     test_of_ty "find"
       DSL.([[data] @-> bool; seq data] --> monad (option data))
       Seq.find
       SeqM.find
   let test_find_map =
     test_of_ty "find_map"
       DSL.([[data] @-> option data; seq data] --> monad (option data))
       Seq.find_map
       SeqM.find_map
   let test_iter2 =
     test_of_ty "iter2"
       DSL.([ [data; data; data] @-> data; data; seq data; seq data; ] --> monad data)
       (fun f init xs ys ->
         let r = ref init in
         Seq.iter2 (fun x y -> r := f !r x y) xs ys;
         !r)
       (fun f init xs ys ->
         let r = ref init in
         Mon.bind
           (SeqM.iter2
             (fun x y ->
                CallerMon.bind
                    (f !r x y)
                    (fun rr -> r := rr; CallerMon.return ()))
             xs
             ys)
           (fun () -> Mon.return !r))
   let test_fold_left2 =
     test_of_ty "fold_left2"
       DSL.([ [data; data; data] @-> data; data; seq data; seq data; ] --> monad data)
       Seq.fold_left2
       SeqM.fold_left2
   let test_for_all2 =
     test_of_ty "for_all2"
       DSL.([[data; data] @-> bool; seq data; seq data] --> monad bool)
       Seq.for_all2
       SeqM.for_all2
   let test_exists2 =
     test_of_ty "exists2"
       DSL.([[data; data] @-> bool; seq data; seq data] --> monad bool)
       Seq.exists2
       SeqM.exists2

   let tests : _ list = [
      test_equal ;
      test_compare ;
      test_iter;
      test_fold_left;
      test_iteri;
      test_fold_lefti ;
      test_for_all ;
      test_exists ;
      test_find ;
      test_find_map ;
      test_iter2 ;
      test_fold_left2;
      test_for_all2 ;
      test_exists2 ;
   ]

end

module MakeTransformers2Tests
   (Support : SUPPORT2)
   (SeqM
: Seqes.Sigs.SEQMON2TRANSFORMERS
    with type ('a, 'e) mon := ('a, 'e) Support.Mon.t
    with type ('a, 'e) callermon := ('a, 'e) Support.CallerMon.t
    with type ('a, 'e) t := ('a, 'e) Support.seqm)
: sig
   val tests : unit Alcotest.test_case list
end
= struct

   open Support

   let test_init =
     test_of_ty "init"
       DSL.([nat; [int] @-> data] --> seq data)
       Seq.init
       SeqM.init
   let test_unfold =
     test_of_ty "unfold"
       DSL.([[int] @-> option (data * int); int] --> seq data)
       Seq.unfold
       SeqM.unfold
   let test_forever =
     test_of_ty "forever"
       DSL.([[unit] @-> data] --> seq data)
       Seq.forever
       SeqM.forever
   let test_iterate =
     test_of_ty "iterate"
       DSL.([[data] @-> data; data] --> seq data)
       Seq.iterate
       SeqM.iterate
   let test_map =
     test_of_ty "map"
       DSL.([[data] @-> data; seq data] --> seq data)
       Seq.map
       SeqM.map
   let test_mapi =
     test_of_ty "mapi"
       DSL.([[int; data] @-> data; seq data] --> seq data)
       Seq.mapi
       SeqM.mapi
   let test_filter =
     test_of_ty "filter"
       DSL.([[data] @-> bool; seq data] --> seq data)
       Seq.filter
       SeqM.filter
   let test_filter_map =
     test_of_ty "filter_map"
       DSL.([[data] @-> (option data); seq data] --> seq data)
       Seq.filter_map
       SeqM.filter_map
   let test_flat_map =
     test_of_ty "flat_map"
       DSL.([[data] @-> seq data; seq data] --> seq data)
       Seq.flat_map
       SeqM.flat_map
   let test_scan =
     test_of_ty "scan"
       DSL.([[data; data] @-> data; data; seq data] --> seq data)
       Seq.scan
       SeqM.scan
   let test_sorted_merge =
     test_of_ty "sorted_merge"
       DSL.([[data; data] @-> int; seq data; seq data] --> seq data)
       Seq.sorted_merge
       SeqM.sorted_merge
   let test_take_while =
     test_of_ty "take_while"
       DSL.([[data] @-> bool; seq data] --> seq data)
       Seq.take_while
       SeqM.take_while
   let test_drop_while =
     test_of_ty "drop_while"
       DSL.([[data] @-> bool; seq data] --> seq data)
       Seq.drop_while
       SeqM.drop_while
   let test_group =
     test_of_ty "group"
       DSL.([[data; data] @-> bool; seq data] --> seq (seq data))
       Seq.group
       SeqM.group
   let test_map2 =
     test_of_ty "map2"
       DSL.([[data; data] @-> data; seq data; seq data] --> seq data)
       Seq.map2
       SeqM.map2
   let test_map_product =
     test_of_ty "map_product"
       DSL.([[data; data] @-> data; seq data; seq data] --> seq data)
       Seq.map_product
       SeqM.map_product
   let test_partition_map =
     test_of_ty "partition_map"
       DSL.([[data] @-> either data data; seq data] --> (seq data * seq data))
       Seq.partition_map
       SeqM.partition_map
   let test_partition =
     test_of_ty "partition"
       DSL.([[data] @-> bool; seq data] --> (seq data * seq data))
       Seq.partition
       SeqM.partition
   let test_of_dispenser =
     test_of_ty "of_dispenser"
       DSL.([[unit] @-> (option data)] --> seq data)
       Seq.of_dispenser
       SeqM.of_dispenser

   module TraversorsTests = MakeTraversors2Tests(Support)(SeqM)

   let tests : unit Alcotest.test_case list =
      TraversorsTests.tests @ [
      test_init ;
      test_unfold ;
      test_forever ;
      test_iterate ;
      test_map ;
      test_mapi ;
      test_filter ;
      test_filter_map ;
      test_flat_map ;
      (*   test_concat_map ; <-- concat_map is an alias for flat_map *)
      test_scan ;
      test_sorted_merge ;
      test_take_while ;
      test_drop_while ;
      test_group ;
      test_map2 ;
      test_map_product ;
      test_partition_map ;
      test_partition ;
      test_of_dispenser ;
   ]

end

module MakeAll2Tests
   (Support : SUPPORT2 with module CallerMon = TestMonads.TransparentId2)
   (SeqM : Seqes.Sigs.SEQMON2ALL
       with type ('a, 'e) mon := ('a, 'e) Support.Mon.t
       with type ('a, 'e) t := ('a, 'e) Support.seqm)
: sig
   val tests : unit Alcotest.test_case list
end
= struct

   open Support

   let test_is_empty =
     test_of_ty "is_empty"
       DSL.([seq data] --> monad bool)
       Seq.is_empty
       SeqM.is_empty
   let test_uncons =
     test_of_ty "uncons"
       DSL.([seq data] --> monad (option (data * seq data)))
       Seq.uncons
       SeqM.uncons
   let test_length =
     test_of_ty "length"
       DSL.([seq data] --> monad int)
       Seq.length
       SeqM.length
   let test_empty =
     test_of_ty "empty"
       DSL.([unit] --> seq data)
       (fun () -> Seq.empty)
       (fun () -> SeqM.empty)
   let test_return =
     test_of_ty "return"
       DSL.([data] --> seq data)
       Seq.return
       SeqM.return
   let test_cons =
     test_of_ty "cons"
       DSL.([data; seq data] --> seq data)
       Seq.cons
       SeqM.cons
   let test_repeat =
     test_of_ty "repeat"
       DSL.([data] --> seq data)
       Seq.repeat
       SeqM.repeat
   let test_cycle =
     test_of_ty "cycle"
       DSL.([seq data] --> seq data)
       Seq.cycle
       SeqM.cycle
   let test_take =
     test_of_ty "take"
       DSL.([nat; seq data] --> seq data)
       Seq.take
       SeqM.take
   let test_drop =
     test_of_ty "drop"
       DSL.([nat; seq data] --> seq data)
       Seq.drop
       SeqM.drop
   let test_transpose =
     test_of_ty "transpose"
       DSL.([seq (seq data)] --> seq (seq data))
       Seq.transpose
       SeqM.transpose
   let test_append =
     test_of_ty "append"
       DSL.([seq data; seq data] --> seq data)
       Seq.append
       SeqM.append
   let test_concat =
     test_of_ty "concat"
       DSL.([seq (seq data)] --> seq data)
       Seq.concat
       SeqM.concat
   let test_zip =
     test_of_ty "zip"
       DSL.([seq data; seq data] --> seq (data * data))
       Seq.zip
       SeqM.zip
   let test_interleave =
     test_of_ty "interleave"
       DSL.([seq data; seq data] --> seq data)
       Seq.interleave
       SeqM.interleave
   let test_product =
     test_of_ty "product"
       DSL.([seq data; seq data] --> seq (data * data))
       Seq.product
       SeqM.product
   let test_unzip =
     test_of_ty "unzip (split)"
       DSL.([seq (data * data)] --> (seq data * seq data))
       Seq.unzip
       SeqM.unzip
   let test_to_dispenser =
     test_of_ty "to_dispenser"
       DSL.([seq data] --> ([unit] ->> (option data)))
       Seq.to_dispenser
       SeqM.to_dispenser
   let test_ints =
     test_of_ty "ints"
       DSL.([int] --> seq int)
       Seq.ints
       SeqM.ints

   module TransformersTests = MakeTransformers2Tests(Support)(SeqM)

   let tests : unit Alcotest.test_case list =
      TransformersTests.tests @ [
      test_is_empty ;
      test_uncons ;
      test_length ;
      test_empty ;
      test_return ;
      test_cons ;
      test_repeat ;
      test_cycle ;
      test_take ;
      test_drop ;
    (* TODO:
      (* [memoize] and [once] are about multiple traversal and side-effects, they
         need a different kind of testing as the other functions. *)
      test_memoize ;
      test_once ;
    *)
      test_transpose ;
      test_append ;
      test_concat ;
      test_zip ;
      test_interleave ;
      test_product ;
      test_unzip ;
    (*   test_split ; <-- split is an alias for unzip *)
      test_to_dispenser ;
      test_ints ;
   ]

end
