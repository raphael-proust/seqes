(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Raphaël Proust                                         *)
(*                                                                        *)
(*   Copyright 2022 Nomadic Labs                                          *)
(*   Copyright 2025 Raphaël Proust                                        *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Tests Seqstd against Stdlib *)

module Make(M: TestMonads.M1)
: sig
   val tests : unit Alcotest.test_case list
end
= struct
   (* module to test: [Seqes.Standard.Make1(..)] *)
   module SeqM
   : Seqes.Sigs.SEQMON1TRAVERSORS
       with type 'a mon := 'a M.t
       with type 'a callermon := 'a M.t
       with type 'a t := 'a Stdlib.Seq.t
   = Seqes.Standard.Make1(M)

   (* Functorised boilerplate *)
   module TestSupport
   : TestHelpers.SUPPORT1
      with module Mon = M
      with module CallerMon = M
      with type 'a seqm = 'a Stdlib.Seq.t
   = TestHelpers.MakeSupport1
      (TestMonads.TransparentId1)
      (M)
      (M)
      (struct
         include Stdlib.Seq
         let of_seq x = x
      end)

   (* Standard functor only returns traversors so that is what we test *)
   module Tests = TestHelpers.MakeTraversors1Tests(TestSupport)(SeqM)
   include Tests
end

module TestsId = Make(TestMonads.OpaqueId1)
module TestsOpt = Make(TestMonads.Option)

let () =
  Alcotest.run
  "Seqes.Standard.Make1"
  [
     ("OpaqueId", TestsId.tests);
     ("Option", TestsOpt.tests);
  ]

