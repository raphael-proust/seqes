Simple
  $ sortermerger lol foo bar
  0: "lol"
  1: "foo"
  2: "bar"

Splits
  $ sortermerger "foo bar lol" foo bar
  0: "foo"
  1: "bar"
  2: "lol"
  3: "foo"
  4: "bar"

Special
  $ sortermerger "foo\" \\ \t bar lol" foo bar
  0: "foo\""
  1: "\\"
  2: "\\t"
  3: "bar"
  4: "lol"
  5: "foo"
  6: "bar"

Looooong
  $ sortermerger foo bar bazzzzz bla bla bla bla bla bla bla bla bla bla lba bla
  0: "foo"
  1: "bar"
  2: "bazzzzz"
  3: "bla"
  4: "bla"
  5: "bla"
  6: "bla"
  7: "bla"
  8: "bla"
  9: "bla"
  10: "bla"
  ERROR(too many words)
