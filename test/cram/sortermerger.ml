module SeqR = Seqes.Monadic.Make2(struct
  type ('a, 'e) t = ('a, 'e) result
  let return = Result.ok
  let bind = Result.bind
end)

let printable c = Char.code c >= 33 && Char.code c <= 126

let sanitize word =
  if word = "" then
    Ok None
  else if String.for_all printable word then
    Ok (Some (Format.asprintf "%S" word))
  else
    Error "invalid character"

let s1 =
  Array.to_seq Sys.argv
  |> Seq.drop 1
  |> Seq.flat_map (fun s -> List.to_seq (String.split_on_char ' ' s))
  |> SeqR.of_seq
  |> SeqR.M.filter_map sanitize

let is =
  SeqR.M.unfold
    (fun acc ->
      if acc > 10 then
        Error "too many words"
      else
        Ok (Some (acc, acc+1)))
    0

let () =
  match SeqR.iter2 (Format.printf "%d: %s\n") is s1 with
  | Ok () -> ()
  | Error msg -> Format.printf "ERROR(%s)\n" msg
